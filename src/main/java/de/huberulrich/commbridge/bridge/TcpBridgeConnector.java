/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.net.SocketFactory;
import java.beans.ExceptionListener;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.BufferOverflowException;
import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * A BridgeConnector that sends Messages via TCP-Connections. It finds all other Buses connected to the local
 * network and forwards them to the BridgeConnectorManager.
 * <p>
 * This BridgeConnector sends Messages unencrypted over the network. Please use the MessagePipe to secure your data.
 */
public class TcpBridgeConnector extends BridgeConnector {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(TcpBridgeConnector.class);

    /**
     * The ExceptionListener attached to this TcpBridgeConnector
     */
    private ExceptionListener mExceptionListener;

    /**
     * The Port used for transmitting Messages
     * mPort + 1 will be used for broadcasting BusIdentifiers
     */
    private int mPort;

    /**
     * The SocketFactory used by this BridgeConnector (required for testing)
     */
    private SocketFactory mSocketFactory;

    /**
     * The listener for Messages
     */
    private final AsynchronousServerSocketChannel messageListener;

    /**
     * The listener for BusIdentifiers
     */
    private final AsynchronousServerSocketChannel broadcastListener;

    /**
     * Map used to map known BusIdentifiers to actual IP-addresses
     */
    private ConcurrentHashMap<BusIdentifier, InetSocketAddress> mKnownBuses = new ConcurrentHashMap<>();

    /**
     * A BridgeConnector that sends Messages via TCP-Connections. It finds all other Buses connected to the local
     * network and forwards them to the BridgeConnectorManager.
     * <p>
     * This BridgeConnector sends Messages unencrypted over the network. Please use the MessagePipe to secure your data.
     *
     * @param port              This port will be used for transmitting Messages (The port port+1 will be used for broadcasting Buses)
     * @param exceptionListener The exceptionListener to notify of any Exceptions
     */
    public TcpBridgeConnector(int port, ExceptionListener exceptionListener) throws IOException {
        mExceptionListener = exceptionListener;
        mPort = port;
        mSocketFactory = SocketFactory.getDefault();

        // Create an AsynchronousServerSocketChannel that will listen for incoming messages
        messageListener = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(mPort));
        // Create an AsynchronousServerSocketChannel that will listen for other hosts
        broadcastListener = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(mPort + 1));
    }

    /**
     * A BridgeConnector that sends Messages via TCP-Connections. It finds all other Buses connected to the local
     * network and forwards them to the BridgeConnectorManager.
     * <p>
     * This BridgeConnector sends Messages unencrypted over the network. Please use the MessagePipe to secure your data.
     * Used for internal testing.
     *
     * @param port              This port will be used for transmitting Messages (The port port+1 will be used for broadcasting Buses)
     * @param exceptionListener The exceptionListener to notify of any Exceptions
     */
    TcpBridgeConnector(int port, ExceptionListener exceptionListener, SocketFactory socketFactory) throws IOException {
        mExceptionListener = exceptionListener;
        mPort = port;
        mSocketFactory = socketFactory;

        // Create an AsynchronousServerSocketChannel that will listen for incoming messages
        messageListener = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(mPort));
        // Create an AsynchronousServerSocketChannel that will listen for other hosts
        broadcastListener = AsynchronousServerSocketChannel.open().bind(new InetSocketAddress(mPort + 1));
    }

    /**
     * Closes the listeners of this Connector to free the ports.
     */
    public void closeListeners() {
        try {
            messageListener.close();
            broadcastListener.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        closeListeners();
    }

    @Override
    public BridgeConnectorPriority getPriority(int dataLength) {
        return BridgeConnectorPriority.INEFFICIENT_FAST;
    }

    @Override
    public String getReadableName() {
        return "TCP BridgeConnector on Port: " + mPort;
    }


    @Override
    protected void onRegisteredToManager() {
        // Listen for Messages
        messageListener.accept(null, new MessageHandler(this, messageListener, mExceptionListener));
        //Listen for Broadcasts
        broadcastListener.accept(null, new BroadcastHandler(broadcastListener, mExceptionListener));

        //Broadcasting this Bus to all other Buses
        sendLocalBusIdentifierToHost("255.255.255.255");
    }

    /**
     * Sends the local BusIdentifier to the specified host, so it adds it to its database.
     *
     * @param host the recipient host
     */
    private void sendLocalBusIdentifierToHost(String host) {
        try {
            Socket socket = mSocketFactory.createSocket(host, mPort + 1);
            OutputStream outputStream = socket.getOutputStream();
            outputStream.write(getLocalBusIdentifier().toString().getBytes(StandardCharsets.UTF_8));
            outputStream.flush();
            outputStream.close();
            socket.close();
        } catch (SocketException e) {
            //ignore
        } catch (IOException | NotRegisteredException e) {
            if (mExceptionListener != null)
                mExceptionListener.exceptionThrown(e);
        }
    }

    @Override
    protected void outgoingMessage(BusIdentifier recipient, UUID messageId, byte[] messageBytes) {
        try {
            //create socket to recipient
            Socket socket = mSocketFactory.createSocket(mKnownBuses.get(recipient).getAddress(), mPort);

            //get stream
            OutputStream outputStream = socket.getOutputStream();
            ByteBuffer messageBuffer = ByteBuffer.allocate(Integer.BYTES);

            //send message length
            messageBuffer.putInt(messageBytes.length);
            outputStream.write(messageBuffer.array());

            //send actual bytes of message
            outputStream.write(messageBytes);

            //flush the stream
            outputStream.flush();
        } catch (UnknownHostException e) {
            //Bus is not reachable anymore!!!
            //Remove this Bus from the list of known Buses
            mKnownBuses.remove(recipient);
            setConnectedBusIdentifiers(new ArrayList<>(mKnownBuses.keySet()));

            //Send the Message back to the Bus so he can find another BridgeConnector that can send it
            try {
                incomingMessage(messageBytes, mExceptionListener);
            } catch (NotRegisteredException e1) {
                if (mExceptionListener != null)
                    mExceptionListener.exceptionThrown(e);
            }
        } catch (IOException e) {
            if (mExceptionListener != null)
                mExceptionListener.exceptionThrown(e);
        }
    }

    /**
     * Handler for receiving incoming Messages
     */
    protected class MessageHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {
        TcpBridgeConnector mTcpBridgeConnector;
        AsynchronousServerSocketChannel mListener;
        ExceptionListener mExceptionListener;

        MessageHandler(TcpBridgeConnector tcpBridgeConnector, AsynchronousServerSocketChannel listener, ExceptionListener exceptionListener) {
            mTcpBridgeConnector = tcpBridgeConnector;
            mListener = listener;
            mExceptionListener = exceptionListener;
        }

        @Override
        public void completed(AsynchronousSocketChannel ch, Void att) {
            // Accept the next connection
            mListener.accept(null, this);

            String logAppend = "[unknown]";
            try {
                logAppend = "[Bus:" + getLocalBusIdentifier() + "][BridgeConnector:" + getReadableName() + "]";
                logAppend += "[" + ch.getRemoteAddress() + "] ";
                log.info(logAppend + "Accepted new connection for incoming Message");

                // Allocate a byte buffer (4K) to read from the client
                ByteBuffer byteBuffer = ByteBuffer.allocate(4096);
                ByteBuffer messageBuffer;

                // Read the first chunk
                int bytesRead = ch.read(byteBuffer).get(20, TimeUnit.SECONDS);
                log.debug(logAppend + "Read first chunk of data with size: " + bytesRead + "B");

                if (bytesRead >= 4) {
                    // Make the buffer ready to read
                    byteBuffer.flip();

                    // Get the length of the message
                    int messageLength = byteBuffer.getInt();
                    log.info(logAppend + "Message length is: " + messageLength + "B");

                    if (messageLength < 0 || messageLength > 21000) { //Actual limit is 20730 for Requests which should be the largest Messages
                        log.error(logAppend + "Message length exceeds limit of 21000B or is negative");
                        ch.write(ByteBuffer.wrap("MessageLength exceeds limit".getBytes(StandardCharsets.UTF_8)));
                    }

                    messageBuffer = ByteBuffer.allocate(messageLength);
                } else {
                    log.error(logAppend + "First chunk of data was not big enough. Closing connection");
                    return;
                }

                //read chunks of data until we have all of the message or no more data is received
                while (bytesRead != -1) {

                    // Read the next chunk
                    messageBuffer.put(byteBuffer);

                    if (messageBuffer.remaining() == 0)
                        break;
                    else
                        log.debug(logAppend + "Remaining data to receive: " + messageBuffer.remaining() + "B");

                    // Make the buffer ready to write
                    byteBuffer.clear();

                    // Read the next line
                    bytesRead = ch.read(byteBuffer).get(20, TimeUnit.SECONDS);
                    log.debug(logAppend + "Received data chunk with size: " + bytesRead + "B");

                    // Make the buffer ready to read
                    byteBuffer.flip();
                }

                //push the finished message to the Bus
                if (messageBuffer.remaining() == 0) {
                    log.info(logAppend + "Received whole message, parsing...");
                    mTcpBridgeConnector.incomingMessage(messageBuffer.array(), mExceptionListener);
                } else
                    log.error(logAppend + "Client stopped sending data, but the message was not transferred completely");
            } catch (IOException | InterruptedException | ExecutionException | NotRegisteredException e) {
                log.error(logAppend + "Internal error", e);
                ch.write(ByteBuffer.wrap("Internal error".getBytes(StandardCharsets.UTF_8)));
                if (mExceptionListener != null)
                    mExceptionListener.exceptionThrown(e);
            } catch (TimeoutException e) {
                // The user exceeded the 20 second timeout, so close the connection
                log.error(logAppend + "Connection timed out");
                ch.write(ByteBuffer.wrap("Timed out".getBytes(StandardCharsets.UTF_8)));
            } catch (BufferOverflowException | IndexOutOfBoundsException e) {
                //The message was longer than initially promised, we abort and notify the sender
                log.error(logAppend + "Message exceeded promised length");
                ch.write(ByteBuffer.wrap("Message exceeded promised length".getBytes(StandardCharsets.UTF_8)));
            }

            log.info(logAppend + "Closing connection");
            closeConnection(ch, mExceptionListener);
        }

        @Override
        public void failed(Throwable exc, Void att) {
            if (exc instanceof AsynchronousCloseException || exc instanceof ClosedChannelException) {
                //These exceptions are common and of no further interest
                return;
            }

            if (exc instanceof Exception)
                if (mExceptionListener != null)
                    mExceptionListener.exceptionThrown((Exception) exc);
        }
    }

    /**
     * Listener for incoming BusIdentifiers
     */
    protected class BroadcastHandler implements CompletionHandler<AsynchronousSocketChannel, Void> {
        AsynchronousServerSocketChannel mListener;
        ExceptionListener mExceptionListener;

        BroadcastHandler(AsynchronousServerSocketChannel listener, ExceptionListener exceptionListener) {
            mListener = listener;
            mExceptionListener = exceptionListener;
        }

        @Override
        public void completed(AsynchronousSocketChannel ch, Void att) {
            // Accept the next connection
            mListener.accept(null, this);

            String logAppend = "[unknown]";
            try {
                logAppend = "[Bus:" + getLocalBusIdentifier() + "][BridgeConnector:" + getReadableName() + "]";
                logAppend += "[" + ch.getRemoteAddress() + "] ";
                log.info(logAppend + "Accepted new connection for incoming BusIdentifier");

                // Allocate a byte buffer
                // (one more byte to see if sender sends busIdentifier that is longer than allowed, and we don't want to
                // simply cut this off)
                ByteBuffer byteBuffer = ByteBuffer.allocate(33);

                // Read the first chunk
                ch.read(byteBuffer).get(20, TimeUnit.SECONDS);
                log.debug(logAppend + "Received data");

                byteBuffer.flip();

                //parse BusIdentifier
                byte[] busIdentifierBytes = new byte[byteBuffer.remaining()];
                byteBuffer.get(busIdentifierBytes, 0, byteBuffer.remaining());
                BusIdentifier busIdentifier = new BusIdentifier(new String(busIdentifierBytes, StandardCharsets.UTF_8));
                log.info(logAppend + "Parsed BusIdentifier: " + busIdentifier);

                if (!busIdentifier.equals(getLocalBusIdentifier())) {
                    //BusIdentifier is of another bus
                    mKnownBuses.put(busIdentifier, (InetSocketAddress) ch.getRemoteAddress());
                    setConnectedBusIdentifiers(new ArrayList<>(mKnownBuses.keySet()));

                    sendLocalBusIdentifierToHost(((InetSocketAddress) ch.getRemoteAddress()).getHostString());
                    log.info(logAppend + "New BusIdentifier added to known Buses: " + busIdentifier);
                } else {
                    //BusIdentifier is our own
                    log.info(logAppend + "Broadcast was our own. Discarding it");
                }
            } catch (InterruptedException | ExecutionException | TimeoutException | IOException | NotRegisteredException e) {
                log.error(logAppend + "Internal error", e);
                ch.write(ByteBuffer.wrap("Internal error".getBytes(StandardCharsets.UTF_8)));
                if (mExceptionListener != null)
                    mExceptionListener.exceptionThrown(e);
            } catch (IllegalArgumentException | BufferOverflowException | IndexOutOfBoundsException e) {
                //The message was longer than initially promised, we abort and notify the sender
                log.error(logAppend + "Identifier exceeded maximum length");
                ch.write(ByteBuffer.wrap("Identifier exceeded maximum length".getBytes(StandardCharsets.UTF_8)));
            }

            log.info(logAppend + "Closing connection");
            closeConnection(ch, mExceptionListener);
        }

        @Override
        public void failed(Throwable exc, Void att) {
            if (exc instanceof AsynchronousCloseException ||
                    exc instanceof ClosedChannelException)
                //These exceptions are common and of no further interest
                return;

            if (exc instanceof Exception)
                if (mExceptionListener != null)
                    mExceptionListener.exceptionThrown((Exception) exc);
        }
    }

    /**
     * Closes the connection (for internal use)
     *
     * @param ch                 the connection
     * @param mExceptionListener the ExceptionListener
     */
    private static void closeConnection(AsynchronousSocketChannel ch, ExceptionListener mExceptionListener) {
        try {
            // Close the connection if we need to
            if (ch.isOpen()) {
                ch.close();
            }
        } catch (IOException e) {
            if (mExceptionListener != null)
                mExceptionListener.exceptionThrown(e);
        }
    }
}
