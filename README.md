# CommBridge

CommBridge is a messaging system, based on a bus architecture. Using the server-client 
paradigm, it is possible to attach multiple Plugins to a single Bus, which can use messages 
to communicate with each other. The messages are kept confidential between the sender and 
recipient. Additionally to confidential point-to-point communication CommBridge supports 
broadcasts, that deliver a message to all Plugins attached to a Bus.

To support communication between multiple devices and/or processes CommBridge introduces
BridgeConnectors. BridgeConnectors are connected to a Bus and transfer messages via different 
protocols, like TCP, Bluetooth, IPC to their counterpart. CommBridge comes with tools to 
ensure the security of messages even when transported over unsecure channels. CommBridge 
supports an unlimited amount of BridgeConnectors and Plugins per Bus, while ensuring fast 
delivery of messages.

**As CommBridge is still in a very early stage of development, the API can change significantly
without prior notice.**

#Usage
CommBridge is very easily added to an existing project. In the following you can find a
description of the basic components of CommBridge, how they play together and how to 
modify them for your own needs.

##Bus
A Bus handles the communication between multiple attached [Plugins](#plugin) and 
[BridgeConnectors](#bridgeconnector).

```java
Bus myBus = new Bus("<identifier>");
```

If you want to handle Messages asynchronously use the AsyncBus:
```java
AsyncBus myBus = new AsyncBus("<identifier>");
```

and stop it with `myBus.stopBus(<timeout>)` when you don't need it anymore, to prevent
memory leaks. The timeout is the time in seconds, the Bus has to finish processing of
already sent request before forcefully stopping it.

When creating a new Bus, you have to provide an identifier, which is used by the 
framework, to identify the Bus globally. Therefore this identifier should be unique.
Identifiers can only contain alphanumeric characters (A-Z, a-z, 0-9).

See the chapters about [Plugins](#plugin) and [BridgeConnectors](#bridgeconnector) for 
details on how to connect them to a Bus.

##Plugin
To create a new Plugin that can communicate via the Bus, you have to extend the Plugin 
class. There will be multiple methods that need to be implemented. See the following code
for further details.

```java
public class MyPlugin extends Plugin {
    @Override
    public PluginIdentifier getPluginIdentifier() {
        /* 
        TODO return an unique PluginIdentifier that is used to address your plugin
        This identifier is used by the framework to address recipients and senders of
        messages. It has to be in the form of two alphanumeric strings with one ":" in
        between. For uniformity's sake, use the format "<vendor>:<plugin>".
        
         */
        return new PluginIdentifier("<vendor>:<plugin>");
    }

    @Override
    public String getUserReadableName() {
        /*
        TODO provide a name which can be used to identify your plugin.
        This name can be used by UI-based projects, to easily identify your plugin. 
        This name should be unique, but this is not enforced. 
        */
        return "<readable name of the plugin>";
    }

    @Override
    public Task[] getCapabilities() {
        /* 
        TODO Return an array of Tasks this plugin will be capable of handling.
        See more in Category Task further down
        */
        return new Task[]{new Task("<name of task>")};
    }

    @Override
    public void processRequest(Request request) {
        /*
        TODO Handle incoming messages in this function.
        You can do long running tasks within this method, but make sure your algorithm
        supports concurrency.
         */
    }
}
```

After creating a Plugin, it can be attached to the bus with the following call:

```java
myBus.getPluginManager().registerPlugin(new MyPlugin());
```

Please take note, that the PluginIdentifier has to be unique per bus. If you really 
want to attach one and the same plugin multiple times to the bus, set the identifier 
dynamically within the constructor, at instantiation of the plugin.

##Messaging
Communication in CommBridge is based on Messages, that can be sent from within Plugins.
A message is one of three types:
- Request: A request with associated data to a plugin to handle a specified task, 
that is within its list of capabilities. Requests are sent via: `pushRequestToBus(...)`
- Acknowledgement: Sent by the framework, to signify that a Request was delivered to
the recipient. It does not mean that the recipient actually did anything with the 
request.
- Response: When the recipient has data to send back to the sender of a Request this
is done via Responses. Responses can be sent via: `pushResponseToBus(...)`

A communication typically starts with a Request. The framework returns an 
Acknowledgement to the sender for each recipient that was found (multiple 
Acknowledgements will be returned for broadcasts for example). Lastly the recipient
can return one or multiple Responses to the sender, if he has data to return.

Exceptions that occur when sending a Message via the Bus are returned using the 
ExceptionListener that was provided when pushing Messages via `pushRequestToBus(...)` or
`pushResponseToBus(...)`. If no such listener is attached, the Exceptions are discarded 
silently.

If a sender wants to listen for Acknowledgements and Responses to a Request he has
sent, he can do so by specifying a ResponseListener when calling `pushRequestToBus(...)`.

```java
public class MyResponseListener implements ResponseListener {
    @Override
    public void onAcknowledgementReceived(Acknowledgement acknowledgement) {
        /*
        TODO Handle here any Acknowledgement you receive for a Request.
        You can do long running tasks within this method, but make sure your algorithm
        supports concurrency.
         */
    }

    @Override
    public void onResponseReceived(Response response) {
        /*
        TODO Handle here any Response you receive for a Request.
        You can do long running tasks within this method, but make sure your algorithm
        supports concurrency.
         */
    }

    @Override
    public void onTimeout() {
        /*
        TODO Handle here any timeouts
        This method will be called, when the listener has existed for a specified time
        (30 seconds) and there has never been received any Acknowledgement or Response.
        This is essentially a sign for one of three things:
            - When the Request was addressed to a specific recipient, that this recipient
              is not attached to the bus or any connected bus
            - When the Request was a broadcast, that no plugin is attached to the bus that
              can handle the Task specified in the Request
            - While transmitting the Request via a BridgeConnector to another Bus, an 
            unrecoverable error occurred and the message was lost. Well programmed 
            BridgeConnectors will resend Messages on failure and not just drop them.
                
        You can do long running tasks within this method, but make sure your algorithm
        supports concurrency.
         */
    }
}
```

See the chapter on [MessagePipe](#messagepipe) for possible security considerations.

##Task
Requests in CommBridge are delivered by looking up the specified recipient, or if 
he is unknown (e.g. in a broadcast), by querying all plugins that can handle the 
specified Task of the Request.

```java
Task task = new Task("<name of task>");
```

A Task is an identifier for a capability of a plugin. For example a plugin handling the 
volume of a system, should be capable of the tasks `VOLUME_UP, VOLUME_DOWN, MUTE, UNMUTE`.
While most plugins will have capabilities, there can also exist plugins that specify
none. These are plugins that only push messages to the bus and will never receive
any. A possible such plugin could be a clock-synchronization plugin, that broadcasts
the current time to all other plugins, so each can synchronize its internal clock.

##BridgeConnector
To connect multiple Buses, CommBridge uses BridgeConnectors. BridgeConnectors are 
special plugins that can be attached to the Bus, which handle communication between 
two or more buses. CommBridge supports indefinitely many BridgeConnectors per Bus, 
which provides the capability of setting up a huge network of communicating buses. 
Since the framework does not specify, how the bridge has to transmit the messages, 
CommBridge is well suited to networks with many different communication protocols. For
example one can connect two devices via a Wi-Fi bridge and a third device via 
Bluetooth.

See the following code snippet for details on how to create your own BridgeConnector:

```java
public class MyBridgeConnector extends BridgeConnector {
    @Override
    public BridgeConnectorPriority getPriority(int dataLength) {
        /*
        TODO Return the priority with which this BridgeConnector would send the Message
		The priority is used by the BridgeConnectorManager, when multiple BridgeConnectors
        exist that connect to one and the same Bus. The choice of BridgeConnector is
        reevaluated for each message.

        Provide your priority for the given length of the message here. The possible priorities
        are:
        - EFFICIENT_FAST,
        - EFFICIENT_SLOW,
        - INEFFICIENT_FAST,
        - INEFFICIENT_SLOW

        Some considerations for the right priority to return:
        - If you have a protocol that is energy intensive no matter how long the message is , but
        can handle very big messages very fast, you should return INEFFICIENT_FAST.
        - If you have a protocol that can send small messages very power efficient but needs to
        exit the power saving mode for longer messages:
            - Return EFFICIENT_FAST or EFFICIENT_SLOW for short messages
            - Return INEFFICIENT_FAST or INEFFICIENT_SLOW for long messages
        */
    }

    @Override
    public String getReadableName() {
        /*
        TODO Provide a good name which can be used to identify your BridgeConnector.
        This name is used for UI-based projects and Logs, to easily identify your BridgeConnector.
        This name should be unique, but this is not enforced.
        */
    }

    @Override
    public void outgoingMessage(BusIdentifier recipient, UUID messageId, byte[] messageBytes) {
        /*
        TODO Handle any outgoing messages by delivering them to the connected BridgeConnector.
        */
    }
}
```

When your connection status changes (your BridgeConnector has lost connection to its
counterpart or an additional host has joined the connection) you can relay this 
information to the framework by calling the following method with the new list of 
identifiers of the connected buses:

```java
setConnectedBusIdentifier(ArrayList<BusIdentifier> mConnectedBusIdentifiers);
```

When you happen to find out that you lost connection to a bus, when trying to deliver a 
message, you can simply push the unmodified message back onto the bus, after updating 
the list of connected buses. The framework will take care of choosing another 
BridgeConnector for delivering the message or notifying the sender, when none exists.

You can push a received message or a message you are unable to deliver via the 
following method to the bus. Additionally you can provide an ExceptionListener to be
notified if an Exception is thrown while CommBridge delivers the Message.

```java
parseMessageAndPushOnBus(...)
```

After creating a BridgeConnector, it can be attached to the bus via the following call:

```java
myBus.getBridgeConnectorManager().registerBridgeConnector(new MyBridgeConnector());
```

#MessagePipe
Messages sent via CommBridge are handled confidentially between sender and recipient 
(as in no other plugin is privy to the existence or the content of the message). While
other plugins are unable to get details on messages not concerning them, BridgeConnectors
are privy to the existence and content of any message delivered via them.

To mitigate this security risk, CommBridge introduces the MessagePipe. The MessagePipe is
used to modify messages with PipePlugins prior to sending them via BridgeConnectors.
By adding a plugin that encrypts the messages, you can securely send them even if you 
cannot trust the BridgeConnector. Please make sure that BridgeConnectors that are
connected, use the same PipePlugins in the exactly same order, or transmission will fail.

Since BridgeConnectors could be using MitM-Attacks to compromise key exchange, make sure
to use key exchange protocols that are save against such attacks.

CommBridge comes with a number of PipePlugins: 

* AESGCMEncryptionPlugin: Provides AES encryption using GCM mode.
  (Depending on hardware support very fast en-/decryption)
* CRC32Plugin: Provides an integrity check for the message
* LZ4CompressionPlugin: Provides compression using the very fast LZ4 algorithm

#License
Copyright 2018 Ulrich Huber

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.