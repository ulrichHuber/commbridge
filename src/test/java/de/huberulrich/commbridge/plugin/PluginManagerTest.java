/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Acknowledgement;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(MockitoJUnitRunner.class)
public class PluginManagerTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNull() {
        new PluginManager(null);
    }

    @Test
    public void testRegisterPlugin() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);

        //verify that Bus is set at registration
        verify(plugin).setBus(bus);

        assertArrayEquals(new Plugin[]{plugin}, pluginManager.getAllRegisteredPlugins());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testRegisterPluginMustBeUnique() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);
        pluginManager.registerPlugin(plugin);
    }

    @Test
    public void testUnregisterPlugin() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);
        pluginManager.unregisterPlugin(plugin);

        //verify that Bus is removed at deregistration
        verify(plugin).setBus(null);

        assertArrayEquals(new Plugin[]{}, pluginManager.getAllRegisteredPlugins());
    }

    @Test
    public void testIsPluginRegistered() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin2")).when(plugin2).getPluginIdentifier();
        Task[] tasks2 = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks2).when(plugin2).getCapabilities();
        Plugin plugin3 = mock(Plugin.class);

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);
        pluginManager.registerPlugin(plugin2);

        assertTrue(pluginManager.isPluginRegistered(plugin));
        assertFalse(pluginManager.isPluginRegistered(plugin3));
    }

    @Test
    public void testGetAllRegisteredPlugins() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin2")).when(plugin2).getPluginIdentifier();
        Task[] tasks2 = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks2).when(plugin2).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);
        pluginManager.registerPlugin(plugin2);

        assertEquals(new HashSet<>(Arrays.asList(plugin, plugin2)), new HashSet<>(Arrays.asList(pluginManager.getAllRegisteredPlugins())));
    }

    @Test
    public void testGetRegisteredPlugin() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin2")).when(plugin2).getPluginIdentifier();
        Task[] tasks2 = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks2).when(plugin2).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);
        pluginManager.registerPlugin(plugin2);

        assertEquals(plugin, pluginManager.getRegisteredPlugin(new PluginIdentifier("mock:plugin")));
        assertNull(pluginManager.getRegisteredPlugin(new PluginIdentifier("mock:nonExistent")));
    }

    @Test
    public void testGetPluginForRequestNoRecipientExists() {
        Bus bus = mock(Bus.class);

        PluginManager pluginManager = new PluginManager(bus);

        Request request = mock(Request.class);
        doReturn(new GlobalIdentifier("abc123:*")).when(request).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(request).getMessageId();
        doReturn(new Task("abc123")).when(request).getTask();

        assertEquals(new ArrayList<>(), pluginManager.getPluginsForMessage(request));
    }

    @Test
    public void testGetPluginForResponse() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);

        assertArrayEquals(new Plugin[]{plugin}, pluginManager.getAllRegisteredPlugins());

        Response response = mock(Response.class);
        doReturn(new GlobalIdentifier("abc123:mock:plugin")).when(response).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(response).getMessageId();
        ArrayList<Plugin> plugins = new ArrayList<>();
        plugins.add(plugin);

        assertEquals(plugins, pluginManager.getPluginsForMessage(response));
    }

    @Test
    public void testGetPluginForResponseNoRecipientExists() {
        Bus bus = mock(Bus.class);

        PluginManager pluginManager = new PluginManager(bus);

        Response response = mock(Response.class);
        doReturn(new GlobalIdentifier("abc123:mock:plugin")).when(response).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(response).getMessageId();

        assertEquals(new ArrayList<>(), pluginManager.getPluginsForMessage(response));
    }

    @Test
    public void testGetPluginForAcknowledgement() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);

        assertArrayEquals(new Plugin[]{plugin}, pluginManager.getAllRegisteredPlugins());

        Acknowledgement acknowledgement = mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:mock:plugin")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();
        ArrayList<Plugin> plugins = new ArrayList<>();
        plugins.add(plugin);

        assertEquals(plugins, pluginManager.getPluginsForMessage(acknowledgement));
    }

    @Test
    public void testGetPluginForAcknowledgementNoRecipientExists() {
        Bus bus = mock(Bus.class);

        PluginManager pluginManager = new PluginManager(bus);

        Acknowledgement acknowledgement = mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:mock:plugin")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();

        assertEquals(new ArrayList<>(), pluginManager.getPluginsForMessage(acknowledgement));
    }

    @Test
    public void testGetPluginForTask() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);

        assertArrayEquals(new Plugin[]{plugin}, pluginManager.getAllRegisteredPlugins());

        Task task = new Task("abc123");
        HashSet<Plugin> plugins = new HashSet<>();
        plugins.add(plugin);

        assertEquals(plugins, pluginManager.getPluginsForTask(task));
    }

    @Test
    public void testGetPluginForTaskNoCapableOneExists() {
        Bus bus = mock(Bus.class);
        Plugin plugin = mock(Plugin.class);
        doReturn(new PluginIdentifier("mock:plugin")).when(plugin).getPluginIdentifier();
        Task[] tasks = new Task[]{new Task("abc123"), new Task("def456")};
        doReturn(tasks).when(plugin).getCapabilities();

        PluginManager pluginManager = new PluginManager(bus);
        pluginManager.registerPlugin(plugin);

        assertArrayEquals(new Plugin[]{plugin}, pluginManager.getAllRegisteredPlugins());

        Task task = new Task("ghi789");

        assertNotNull(pluginManager.getPluginsForTask(task));
    }
}
