/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.identifier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Identifier for a Bus
 */
public final class BusIdentifier {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(BusIdentifier.class);
    /**
     * The actual identifier
     */
    private final String mBusIdentifier;

    /**
     * Constructs a new bus-identifier
     *
     * @param identifier the identifier with only:
     *                   - alphanumeric characters
     *                   - or "*" for a global broadcast (all buses)
     *                   - or "-" for a external broadcast (all buses except the local bus)
     */
    public BusIdentifier(String identifier) {
        if (identifier == null || !identifier.matches("^(?:[A-Za-z0-9]+|\\*|-)$")) {
            log.error("Invalid bus-identifier: " + identifier);
            throw new IllegalArgumentException("The provided identifier is in an invalid format");
        }

        if (identifier.length() > 32) {
            log.error("New identifier exceeds maximal length: " + identifier);
            throw new IllegalArgumentException("The provided identifier exceeds the maximal length of 32");
        }

        mBusIdentifier = identifier;
    }

    /**
     * Whether the identifier matches any bus including the local one
     *
     * @return Whether it is a global broadcast
     */
    public boolean isGlobalBroadcast() {
        return mBusIdentifier.equals("*");
    }

    /**
     * Whether the identifier matches any bus excluding the local one
     *
     * @return Whether it is a external broadcast
     */
    public boolean isExternalBroadcast() {
        return mBusIdentifier.equals("-");
    }

    @Override
    public String toString() {
        return mBusIdentifier;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BusIdentifier && obj.toString().equals(toString());
    }

    @Override
    public int hashCode() {
        return mBusIdentifier.hashCode();
    }
}
