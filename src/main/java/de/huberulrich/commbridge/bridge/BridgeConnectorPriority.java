/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

/**
 * Priorities a {@link BridgeConnector} can return in {@link BridgeConnector#getPriority(int)}
 */
public enum BridgeConnectorPriority {
    /**
     * power-efficient and fast transmission
     */
    EFFICIENT_FAST,

    /**
     * power-efficient but slow transmission
     */
    EFFICIENT_SLOW,

    /**
     * power-inefficient but fast transmission
     */
    INEFFICIENT_FAST,

    /**
     * power-inefficient and slow transmission
     */
    INEFFICIENT_SLOW
}