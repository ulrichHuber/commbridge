/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.exceptions.RecipientBusNotFoundException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.message.Acknowledgement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@SuppressWarnings("ResultOfMethodCallIgnored")
@RunWith(MockitoJUnitRunner.class)
public class BridgeConnectorManagerTest {

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNull() {
        new BridgeConnectorManager(null);
    }

    @Test
    public void testRegisterBridgeConnector() throws RecipientBusNotFoundException, PipeException {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();
        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(bridgeConnector).getPriority(anyInt());

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);

        assertArrayEquals(new BridgeConnector[]{bridgeConnector}, bridgeConnectorManager.getAllRegisteredBridges());

        Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:*")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();
        bridgeConnectorManager.outgoingMessage(acknowledgement);

        verify(bridgeConnector).pushThroughPipeToOutgoing(any());
    }

    @Test(expected = RecipientBusNotFoundException.class)
    public void testUnregisterBridgeConnector() throws RecipientBusNotFoundException, PipeException {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);
        bridgeConnectorManager.unregisterBridgeConnector(bridgeConnector);

        assertArrayEquals(new BridgeConnector[]{}, bridgeConnectorManager.getAllRegisteredBridges());

        Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:*")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();
        bridgeConnectorManager.outgoingMessage(acknowledgement);

        verifyNoMoreInteractions(bridgeConnector);
    }

    @Test
    public void testGetConnectedBusIdentifier() {
        Bus bus = Mockito.mock(Bus.class);
        doReturn(new BusIdentifier("abc123")).when(bus).getBusIdentifier();
        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);

        assertEquals(new BusIdentifier("abc123"), bridgeConnectorManager.getConnectedBusIdentifier());
    }

    @Test
    public void testBridgeConnectionStatusChanged() {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(bridgeConnector).getPriority(anyInt());
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);

        busIdentifiers.clear();
        busIdentifiers.add(new BusIdentifier("def456"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();

        bridgeConnectorManager.bridgeConnectionStatusChanged(bridgeConnector);

        try {
            Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
            doReturn(new GlobalIdentifier("abc123:*")).when(acknowledgement).getRecipientIdentifier();
            doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();
            bridgeConnectorManager.outgoingMessage(acknowledgement);
            fail();
        } catch (RecipientBusNotFoundException ignored) {
        } catch (PipeException e) {
            fail();
        }

        try {
            Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
            doReturn(new GlobalIdentifier("def456:*")).when(acknowledgement).getRecipientIdentifier();
            doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();
            bridgeConnectorManager.outgoingMessage(acknowledgement);
        } catch (RecipientBusNotFoundException | PipeException e) {
            fail();
        }
    }

    @Test
    public void testSendMessageToBus() {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.sendMessageToBus(new byte[]{}, null);
        verify(bus).sendMessage((byte[]) any(), any());
    }

    @Test
    public void testOutgoingBroadcastMessage() throws CloneNotSupportedException, PipeException, RecipientBusNotFoundException {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(bridgeConnector).getPriority(anyInt());
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        busIdentifiers.add(new BusIdentifier("def456"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);

        UUID messageId = UUID.randomUUID();
        Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("*:*")).when(acknowledgement).getRecipientIdentifier();
        doReturn(messageId).when(acknowledgement).getMessageId();

        Acknowledgement acknowledgementABC = Mockito.mock(Acknowledgement.class);
        Acknowledgement acknowledgementDEF = Mockito.mock(Acknowledgement.class);

        doReturn(acknowledgementABC).when(acknowledgement).cloneWithNewBusIdentifier(new BusIdentifier("abc123"));
        doReturn(acknowledgementDEF).when(acknowledgement).cloneWithNewBusIdentifier(new BusIdentifier("def456"));

        bridgeConnectorManager.outgoingMessage(acknowledgement);

        verify(bridgeConnector).pushThroughPipeToOutgoing(acknowledgementABC);
        verify(bridgeConnector).pushThroughPipeToOutgoing(acknowledgementDEF);
    }

    @Test
    public void testOutgoingDirectMessage() throws RecipientBusNotFoundException, PipeException {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(bridgeConnector).getPriority(anyInt());
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);

        Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:*")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();

        bridgeConnectorManager.outgoingMessage(acknowledgement);

        verify(bridgeConnector).pushThroughPipeToOutgoing(acknowledgement);
    }

    @Test
    public void testPrioritySelection() throws RecipientBusNotFoundException, PipeException {
        Bus bus = Mockito.mock(Bus.class);
        BridgeConnector bridgeConnector = Mockito.mock(BridgeConnector.class);
        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(bridgeConnector).getPriority(anyInt());
        BridgeConnector bridgeConnector2 = Mockito.mock(BridgeConnector.class);
        doReturn(BridgeConnectorPriority.EFFICIENT_SLOW).when(bridgeConnector2).getPriority(anyInt());
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        doReturn(busIdentifiers).when(bridgeConnector).getConnectedBusIdentifiers();
        doReturn(busIdentifiers).when(bridgeConnector2).getConnectedBusIdentifiers();

        BridgeConnectorManager bridgeConnectorManager = new BridgeConnectorManager(bus);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector);
        bridgeConnectorManager.registerBridgeConnector(bridgeConnector2);

        Acknowledgement acknowledgement = Mockito.mock(Acknowledgement.class);
        doReturn(new GlobalIdentifier("abc123:*")).when(acknowledgement).getRecipientIdentifier();
        doReturn(UUID.randomUUID()).when(acknowledgement).getMessageId();

        bridgeConnectorManager.outgoingMessage(acknowledgement);

        verify(bridgeConnector).pushThroughPipeToOutgoing(acknowledgement);
        verify(bridgeConnector2, never()).pushThroughPipeToOutgoing(acknowledgement);
    }
}
