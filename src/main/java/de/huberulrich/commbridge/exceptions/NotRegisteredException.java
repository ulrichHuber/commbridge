/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.exceptions;

/**
 * This is thrown when no bus is connected to a Plugin or BridgeConnector but the call a method that relies on the bus
 */
public class NotRegisteredException extends Exception {
    /**
     * Constructs a new NotRegisteredException
     */
    public NotRegisteredException() {
    }
}
