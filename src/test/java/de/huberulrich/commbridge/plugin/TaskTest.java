/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class TaskTest {
    @Test
    public void testValidIdentifier() {
        new Task("123");
        new Task("abc123");
        new Task("ABCabc123");
    }

    @Test
    public void testMaximumIdentifierLength() {
        new Task("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyIdentifier() {
        new Task("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier() {
        new Task(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDisallowedCharacterIdentifier() {
        new Task(".");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooLongIdentifier() {
        new Task("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLineBreakIdentifier() {
        new Task("\n");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidWithWhitespaceIdentifier() {
        new Task("abc ");
    }

    @Test
    public void testToString() {
        assertEquals("abc", new Task("abc").toString());
    }

    @Test
    public void testEquals() {
        assertEquals(new Task("abc"), new Task("abc"));
        assertNotEquals(new Task("abc"), new Task("def"));
    }

    @Test
    public void testRepeatableHashCode() {
        assertEquals(new Task("abc").hashCode(), new Task("abc").hashCode());
        assertNotEquals(new Task("abc").hashCode(), new Task("def").hashCode());
    }
}
