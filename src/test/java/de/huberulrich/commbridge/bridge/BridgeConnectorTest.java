/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import de.huberulrich.commbridge.bridge.pipe.MessagePipe;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.message.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class BridgeConnectorTest {
    public BridgeConnectorTest() {

    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetConnectedBusIdentifiersWithNull() {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());

        bridgeConnector.setConnectedBusIdentifiers(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetConnectedBusIdentifiersWithGlobalBroadcastIdentifier() {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());

        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("*"));
        bridgeConnector.setConnectedBusIdentifiers(busIdentifiers);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testSetConnectedBusIdentifiersWithExternalBroadcastIdentifier() {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());

        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("-"));
        bridgeConnector.setConnectedBusIdentifiers(busIdentifiers);
    }

    @Test
    public void testSetConnectedBusIdentifiersWhenNotConnectedToManager() {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());

        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        busIdentifiers.add(new BusIdentifier("def456"));
        bridgeConnector.setConnectedBusIdentifiers(busIdentifiers);

        assertEquals(busIdentifiers, bridgeConnector.getConnectedBusIdentifiers());
    }

    @Test
    public void testSetConnectedBusIdentifiersWhenConnectedToManager() {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);

        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>();
        busIdentifiers.add(new BusIdentifier("abc123"));
        busIdentifiers.add(new BusIdentifier("def456"));
        bridgeConnector.setConnectedBusIdentifiers(busIdentifiers);
        verify(bridgeConnectorManager).bridgeConnectionStatusChanged(bridgeConnector);

        assertEquals(busIdentifiers, bridgeConnector.getConnectedBusIdentifiers());
    }

    @Test
    public void testGetLocalBusIdentifier() throws NotRegisteredException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        doReturn(new BusIdentifier("testBus")).when(bridgeConnectorManager).getConnectedBusIdentifier();

        assertEquals(new BusIdentifier("testBus"), bridgeConnector.getLocalBusIdentifier());
    }

    @Test(expected = NotRegisteredException.class)
    public void testGetLocalBusIdentifierWhenNotConnected() throws NotRegisteredException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());

        bridgeConnector.getLocalBusIdentifier();
    }

    @Test(expected = IllegalAccessException.class)
    public void testGetMessagePipeWithWrongManagerFails() throws IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        bridgeConnector.getMessagePipe(null);
    }

    @Test
    public void testSetMessagePipe() throws IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        MessagePipe messagePipe = Mockito.mock(MessagePipe.class);
        bridgeConnector.setMessagePipe(null, messagePipe);
        assertEquals(messagePipe, bridgeConnector.getMessagePipe(null));
    }

    @Test(expected = IllegalAccessException.class)
    public void testSetMessagePipeWithWrongManagerFails() throws IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        MessagePipe messagePipe = Mockito.mock(MessagePipe.class);
        bridgeConnector.setMessagePipe(null, messagePipe);
    }

    @Test
    public void testRemoveMessagePipe() throws IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        bridgeConnector.removeMessagePipe(bridgeConnectorManager);
        assertNull(bridgeConnector.getMessagePipe(bridgeConnectorManager));
    }

    @Test(expected = IllegalAccessException.class)
    public void testRemoveMessagePipeWithWrongManagerFails() throws IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        bridgeConnector.removeMessagePipe(null);
    }

    @Test
    public void testPushThroughPipeToOutgoing() throws PipeException, IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        MessagePipe messagePipe = Mockito.mock(MessagePipe.class);

        bridgeConnector.setMessagePipe(bridgeConnectorManager, messagePipe);

        Request request = mock(Request.class, Mockito.RETURNS_DEEP_STUBS);
        bridgeConnector.pushThroughPipeToOutgoing(request);

        InOrder inOrder = Mockito.inOrder(messagePipe, bridgeConnector);
        inOrder.verify(messagePipe).applyPipe(any());
        inOrder.verify(bridgeConnector).outgoingMessage(any(), any(), any());
    }

    @Test
    public void testForwardMessageToBus() throws PipeException, NotRegisteredException, IllegalAccessException {
        BridgeConnector bridgeConnector = Mockito.spy(new BridgeConnectorImpl());
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        bridgeConnector.setBridgeConnectorManager(bridgeConnectorManager);
        MessagePipe messagePipe = Mockito.mock(MessagePipe.class);

        bridgeConnector.setMessagePipe(bridgeConnectorManager, messagePipe);

        bridgeConnector.incomingMessage(new byte[]{}, null);

        InOrder inOrder = Mockito.inOrder(messagePipe, bridgeConnectorManager);
        inOrder.verify(messagePipe).undoPipe(any());
        inOrder.verify(bridgeConnectorManager).sendMessageToBus(any(), any());
    }

    private class BridgeConnectorImpl extends BridgeConnector {

        @Override
        public BridgeConnectorPriority getPriority(int dataLength) {
            return null;
        }

        @Override
        public String getReadableName() {
            return "TestingBridgeConnector";
        }

        @Override
        protected void outgoingMessage(BusIdentifier recipient, UUID messageId, byte[] messageBytes) {

        }
    }
}
