/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bus;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.bridge.BridgeConnectorManager;
import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.exceptions.RecipientBusNotFoundException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Acknowledgement;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.Response;
import de.huberulrich.commbridge.plugin.Plugin;
import de.huberulrich.commbridge.plugin.PluginManager;
import de.huberulrich.commbridge.plugin.Task;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mockito;

import java.beans.ExceptionListener;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

public class BusTest {
    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNull() {
        new Bus(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithGlobalBroadcastIdentifier() {
        new Bus(new BusIdentifier("*"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithExternalBroadcastIdentifier() {
        new Bus(new BusIdentifier("-"));
    }

    @Test
    public void testGetBusIdentifier() {
        assertEquals(new BusIdentifier("abc123"), new Bus(new BusIdentifier("abc123")).getBusIdentifier());
    }

    @Test
    public void testGetPluginManager() {
        assertNotNull(new Bus(new BusIdentifier("abc123")).getPluginManager());
    }

    @Test
    public void testGetBridgeConnectorManager() {
        assertNotNull(new Bus(new BusIdentifier("abc123")).getBridgeConnectorManager());
    }

    @Test
    public void testSendGlobalBroadcast() throws RecipientBusNotFoundException, PipeException {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("def:ghi")).when(plugin2).getPluginIdentifier();
        plugins.add(plugin2);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Request request = mock(Request.class);
        doReturn(new GlobalIdentifier("*:*")).when(request).getRecipientIdentifier();
        doReturn(new GlobalIdentifier("def456:abc:def")).when(request).getSenderIdentifier();
        doReturn(Generators.timeBasedGenerator().generate()).when(request).getMessageId();

        bus.sendMessage(request, e -> fail());
        verify(plugin1).processRequest(request);
        verify(plugin2).processRequest(request);
        verify(bridgeConnectorManager).outgoingMessage(request);
        InOrder inOrder = Mockito.inOrder(bus);
        inOrder.verify(bus, Mockito.calls(2)).sendMessage((Acknowledgement) any(), any());
    }

    @Test
    public void testSendExternalBroadcast() throws RecipientBusNotFoundException, PipeException {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Request request = mock(Request.class);
        doReturn(new GlobalIdentifier("-:*")).when(request).getRecipientIdentifier();

        bus.sendMessage(request, e -> fail());
        verifyNoMoreInteractions(plugin1);
        verify(bridgeConnectorManager).outgoingMessage(request);
    }

    @Test
    public void testSendDirectInternalMessage() throws RecipientBusNotFoundException, PipeException {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Request request = mock(Request.class);
        doReturn(new GlobalIdentifier("abc123:abc:def")).when(request).getRecipientIdentifier();
        doReturn(new GlobalIdentifier("def456:abc:def")).when(request).getSenderIdentifier();
        doReturn(Generators.timeBasedGenerator().generate()).when(request).getMessageId();

        bus.sendMessage(request, e -> fail());
        verify(plugin1).processRequest(request);
        InOrder inOrderBCM = Mockito.inOrder(bridgeConnectorManager);
        inOrderBCM.verify(bridgeConnectorManager, Mockito.never()).outgoingMessage(request);
        inOrderBCM.verify(bridgeConnectorManager, Mockito.calls(1)).outgoingMessage(any());
    }

    @Test
    public void testSendDirectExternalMessage() throws RecipientBusNotFoundException, PipeException {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Request request = mock(Request.class);
        doReturn(new GlobalIdentifier("ghi789:abc:def")).when(request).getRecipientIdentifier();
        doReturn(new GlobalIdentifier("def456:abc:def")).when(request).getSenderIdentifier();
        doReturn(Generators.timeBasedGenerator().generate()).when(request).getMessageId();

        bus.sendMessage(request, e -> fail());
        verify(pluginManager, never()).getPluginsForMessage(any());
        verify(bridgeConnectorManager).outgoingMessage(request);
    }

    @Test
    public void sendUnparsedRequest() {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Request request = new Request(new GlobalIdentifier("def:ghi:jkl"), new GlobalIdentifier("abc123:abc:def"), new Task("abc"), new byte[]{});
        bus.sendMessage(request.getBytes(), e -> fail());
        verify(plugin1).processRequest(any());
        verify(bus).sendMessage((Acknowledgement) any(), any());
    }



    @Test
    public void sendUnparsedResponse() {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Response response = new Response(new GlobalIdentifier("def:ghi:jkl"), new GlobalIdentifier("abc123:abc:def"), UUID.randomUUID(), new byte[]{});
        bus.sendMessage(response.getBytes(), e -> fail());
        verify(plugin1).processResponse(any());
        verify(bus).sendMessage((Acknowledgement) any(), any());
    }

    @Test
    public void sendUnparsedAcknowledgement() {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("def:ghi:jkl"), new GlobalIdentifier("abc123:abc:def"), UUID.randomUUID());
        bus.sendMessage(acknowledgement.getBytes(), e -> fail());
        verify(plugin1).processResponse(any());
        verifyZeroInteractions(bridgeConnectorManager);
    }

    @Test
    public void sendUnparsedMessageWithUnknownType() {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        ExceptionListener e = mock(ExceptionListener.class);
        bus.sendMessage(new byte[]{-1}, e);
        verify(e, Mockito.timeout(10000)).exceptionThrown(any(ParseMessageException.class));
    }

    @Test
    public void sendUnparsedMessageWithEmptyArray() {
        Bus bus = spy(new Bus(new BusIdentifier("abc123")));
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        PluginManager pluginManager = mock(PluginManager.class);
        ArrayList<Plugin> plugins = new ArrayList<>();
        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:def")).when(plugin1).getPluginIdentifier();
        plugins.add(plugin1);
        doReturn(plugins).when(pluginManager).getPluginsForMessage(any());
        bus.injectManagers(bridgeConnectorManager, pluginManager);

        ExceptionListener e = mock(ExceptionListener.class);
        bus.sendMessage(new byte[]{}, e);
        verify(e, Mockito.timeout(10000)).exceptionThrown(any(ParseMessageException.class));
    }
}
