/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Acknowledgement;
import de.huberulrich.commbridge.message.Message;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.Response;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The PluginManager handles organization of all plugins registered to it.
 * Its main purpose is to find the appropriate plugins for a task.
 */
public class PluginManager {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(PluginManager.class);

    /**
     * Internal synchronization object
     */
    private ReadWriteLock lock = new ReentrantReadWriteLock(true);

    /**
     * Map with all registered plugins identified by available tasks
     */
    private final HashSetValuedHashMap<de.huberulrich.commbridge.plugin.Task, Plugin> mPluginsByTask;

    /**
     * Map with all registered plugins identified by their identifier
     */
    private final ConcurrentHashMap<PluginIdentifier, Plugin> mPluginsById;

    /**
     * The bus this manager is connected to
     */
    private final Bus mBus;

    /**
     * Constructs a new PluginManager associated to the provided Bus
     *
     * @param bus the Bus
     */
    public PluginManager(Bus bus) {
        if (bus == null)
            throw new IllegalArgumentException("Bus cannot be null");

        mPluginsByTask = new HashSetValuedHashMap<>();
        mPluginsById = new ConcurrentHashMap<>();
        mBus = bus;
        log.info("[Bus:" + mBus.getBusIdentifier() + "] New PluginManager created");
    }

    /**
     * Register the plugin
     *
     * @param plugin the plugin
     */
    public void registerPlugin(Plugin plugin) {
        log.info("[Bus:" + mBus.getBusIdentifier() + "][PluginManager] Registering new Plugin: " + plugin.getPluginIdentifier());

        if (mPluginsById.containsKey(plugin.getPluginIdentifier())) {
            log.error("[Bus:" + mBus.getBusIdentifier() + "][PluginManager][Plugin:" + plugin.getPluginIdentifier() + "] Duplicate Plugin");
            throw new IllegalArgumentException("A plugin with this identifier is already registered");
        }

        //set the bus for the plugin, so it can communicate
        plugin.setBus(mBus);

        //Add plugin to tasks map
        lock.writeLock().lock();
        try {
            for (de.huberulrich.commbridge.plugin.Task task : plugin.getCapabilities()) {
                mPluginsByTask.put(task, plugin);
            }
        } finally {
            lock.writeLock().unlock();
        }

        //Add plugin to id map
        mPluginsById.put(plugin.getPluginIdentifier(), plugin);

        log.debug("[Bus:" + mBus.getBusIdentifier() + "][PluginManager][Plugin:" + plugin.getPluginIdentifier() + "] Plugin registered");

    }

    /**
     * Unregister a plugin from the manager
     *
     * @param plugin the plugin
     */
    public void unregisterPlugin(Plugin plugin) {
        lock.writeLock().lock();
        try {
            log.info("[Bus:" + mBus.getBusIdentifier() + "][PluginManager][Plugin:" + plugin.getPluginIdentifier() + "] Unregistering plugin");
            //remove plugin from tasks map
            for (de.huberulrich.commbridge.plugin.Task task : plugin.getCapabilities()) {
                mPluginsByTask.removeMapping(task, plugin);
            }

            //Remove plugin from id map
            mPluginsById.remove(plugin.getPluginIdentifier());

            plugin.setBus(null);

            log.debug("[Bus:" + mBus.getBusIdentifier() + "][PluginManager][Plugin:" + plugin.getPluginIdentifier() + "] Plugin unregistered");
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Check if a plugin is registered
     *
     * @param plugin the plugin
     */
    public boolean isPluginRegistered(Plugin plugin) {
        return mPluginsById.containsValue(plugin);
    }

    /**
     * Returns all registered Plugins
     *
     * @return Array with all Plugins that are registered
     */
    public Plugin[] getAllRegisteredPlugins() {
        Plugin[] plugins = new Plugin[mPluginsById.size()];
        return mPluginsById.values().toArray(plugins);
    }

    /**
     * Returns the Plugin identified by the provided PluginIdentifier, if it is registered with this PluginManager
     *
     * @return The Plugin or null
     */
    public Plugin getRegisteredPlugin(PluginIdentifier identifier) {
        return mPluginsById.get(identifier);
    }


    /**
     * Get all plugins for a specific task
     *
     * @param task the task
     */
    public Collection<Plugin> getPluginsForTask(de.huberulrich.commbridge.plugin.Task task) {
        lock.readLock().lock();
        try {
            log.debug("[Bus:" + mBus.getBusIdentifier() + "][PluginManager] Querying plugins for Task: " + task);

            Collection<Plugin> plugins = mPluginsByTask.get(task);

            log.trace("[Bus:" + mBus.getBusIdentifier() + "][PluginManager] Query resulted in:\n" + plugins.toString());

            return plugins;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Get the plugin for a specific id if it is registered with this PluginManager
     *
     * @param identifier the identifier of the plugin
     * @return the plugin if registered or null
     */
    private Plugin getPluginForIdentifier(PluginIdentifier identifier) {
        log.debug("[Bus:" + mBus.getBusIdentifier() + "][PluginManager] Querying plugins for identifier: " + identifier);

        Plugin plugin = mPluginsById.get(identifier);

        log.trace("[Bus:" + mBus.getBusIdentifier() + "][PluginManager] Query resulted in:\n" + plugin);

        return plugin;
    }

    /**
     * Returns a list of Plugins appropriate for the message
     *
     * @param message the message
     * @return the list with all appropriate plugins, or an empty list
     */
    public ArrayList<Plugin> getPluginsForMessage(Message message) {
        String logAppend = "[Bus:" + mBus.getBusIdentifier() + "][ID:" + message.getMessageId() + "][PluginManager] ";
        log.debug(logAppend + "Querying plugins for message");

        ArrayList<Plugin> resultList;
        if (message instanceof Request) {
            if (message.getRecipientIdentifier().getPluginIdentifier().isBroadcast()) {
                Collection<Plugin> pluginCollection = getPluginsForTask(((Request) message).getTask());
                resultList = new ArrayList<>(pluginCollection.size());
                resultList.addAll(pluginCollection);
            } else {
                resultList = new ArrayList<>(1);
                Plugin plugin = getPluginForIdentifier(message.getRecipientIdentifier().getPluginIdentifier());
                if (plugin != null && getPluginsForTask(((Request) message).getTask()).contains(plugin))
                    resultList.add(plugin);
            }
        } else if (message instanceof Acknowledgement || message instanceof Response) {
            resultList = new ArrayList<>(1);
            Plugin plugin = getPluginForIdentifier(message.getRecipientIdentifier().getPluginIdentifier());
            if (plugin != null)
                resultList.add(plugin);
        } else {
            log.error(logAppend + "Message is of unknown type");
            resultList = new ArrayList<>(0);
        }

        if (resultList.isEmpty())
            log.warn(logAppend + "No appropriate plugin found");
        else
            log.trace(resultList.toString());

        return resultList;
    }
}
