/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.exceptions.PipeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class CRC32PluginTest {
    private CRC32Plugin crc32Plugin;
    private byte[] messageBytes;

    public CRC32PluginTest() {
        Random random = new Random(Generators.timeBasedGenerator().generate().getMostSignificantBits());
        crc32Plugin = new CRC32Plugin();
        messageBytes = new byte[400];
        random.nextBytes(messageBytes);
    }

    @Test
    public void testApplyAndUndoAreMatchingOperations() {
        try {
            assertArrayEquals(messageBytes, crc32Plugin.undo(crc32Plugin.apply(messageBytes)));
        } catch (PipeException e) {
            fail();
        }
    }

    @Test(expected = PipeException.class)
    public void testMessageCorruption() throws PipeException {
            byte[] appliedBytes = crc32Plugin.apply(messageBytes);
            appliedBytes[2] = (byte) ((appliedBytes[2] + 5) % Byte.MAX_VALUE);
            crc32Plugin.undo(appliedBytes);
    }
}
