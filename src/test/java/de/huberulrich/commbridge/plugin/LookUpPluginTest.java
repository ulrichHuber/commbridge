/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bridge.BridgeConnectorManager;
import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Message;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.Response;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.TreeSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class LookUpPluginTest {

    @Test
    public void testInstantiationWithNull() {
        try {
            new LookUpPlugin(null, null, true);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new LookUpPlugin(mock(PluginManager.class), null, true);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new LookUpPlugin(null, mock(BridgeConnectorManager.class), true);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testLookUpExternalProhibitedWhenSet() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("external:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpPluginsTask,
                null);

        lookUpPlugin.processRequest(request);
        verify(bus, never()).sendMessage(any(Message.class), any());
    }

    @Test
    public void testAllowExternalQueries() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("a:b")).when(plugin1).getPluginIdentifier();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("b:c")).when(plugin2).getPluginIdentifier();
        doReturn(new Plugin[]{plugin1, plugin2}).when(pluginManager).getAllRegisteredPlugins();

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("external:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpPluginsTask,
                null);

        lookUpPlugin.allowExternalQueries();
        lookUpPlugin.processRequest(request);
        verify(bus).sendMessage(any(Message.class), any());
    }

    @Test
    public void testDisallowExternalQueries() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);

        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, true);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("external:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpPluginsTask,
                null);

        lookUpPlugin.disallowExternalQueries();
        lookUpPlugin.processRequest(request);
        verify(bus, never()).sendMessage(any(Message.class), any());
    }

    @Test
    public void testProcessLookUpKnownExternalBusesTask() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        BusIdentifier busIdentifier1 = mock(BusIdentifier.class);
        doReturn("abc").when(busIdentifier1).toString();
        BusIdentifier busIdentifier2 = mock(BusIdentifier.class);
        doReturn("def").when(busIdentifier2).toString();
        doReturn(new BusIdentifier[]{busIdentifier1, busIdentifier2}).when(bridgeConnectorManager).getAllKnownExternalBusIdentifiers();

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpKnownExternalBusesTask,
                null);

        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus).sendMessage(responseCaptor.capture(), any());

        assertEquals("abc,def", new String(responseCaptor.getValue().getData(), StandardCharsets.UTF_8));
    }

    @Test
    public void testProcessLookUpKnownExternalBusesTaskWithNonKnown() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier[]{}).when(bridgeConnectorManager).getAllKnownExternalBusIdentifiers();

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpKnownExternalBusesTask,
                null);

        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus).sendMessage(responseCaptor.capture(), any());

        assertEquals("", new String(responseCaptor.getValue().getData(), StandardCharsets.UTF_8));
    }

    @Test
    public void testProcessLookUpPluginsTask() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:123")).when(plugin1).getPluginIdentifier();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("def:456")).when(plugin2).getPluginIdentifier();
        doReturn(new Plugin[]{plugin1, plugin2}).when(pluginManager).getAllRegisteredPlugins();

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpPluginsTask,
                null);

        lookUpPlugin.disallowExternalQueries();
        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus).sendMessage(responseCaptor.capture(), any());

        assertEquals("abc:123,def:456", new String(responseCaptor.getValue().getData(), StandardCharsets.UTF_8));
    }


    @Test
    public void testProcessLookUpPluginsWithCapabilityTask() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        Plugin plugin1 = mock(Plugin.class);
        doReturn(new PluginIdentifier("abc:123")).when(plugin1).getPluginIdentifier();
        Plugin plugin2 = mock(Plugin.class);
        doReturn(new PluginIdentifier("def:456")).when(plugin2).getPluginIdentifier();
        TreeSet<Plugin> plugins = new TreeSet<>(Comparator.comparing(o -> o.getPluginIdentifier().toString()));
        plugins.add(plugin1);
        plugins.add(plugin2);

        doReturn(plugins).when(pluginManager).getPluginsForTask(any());

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpPluginsWithCapabilityTask,
                new Task("abc").getBytes());

        lookUpPlugin.disallowExternalQueries();
        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus).sendMessage(responseCaptor.capture(), any());

        assertEquals("abc:123,def:456", new String(responseCaptor.getValue().getData(), StandardCharsets.UTF_8));
    }

    @Test
    public void testProcessLookUpCapabilitiesTask() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        Plugin plugin = mock(Plugin.class);
        doReturn(new Task[]{new Task("abc"), new Task("def")}).when(plugin).getCapabilities();
        doReturn(plugin).when(pluginManager).getRegisteredPlugin(new PluginIdentifier("abc:123"));

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpCapabilitiesTask,
                new PluginIdentifier("abc:123").getBytes());

        lookUpPlugin.disallowExternalQueries();
        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus).sendMessage(responseCaptor.capture(), any());

        assertEquals("abc,def", new String(responseCaptor.getValue().getData(), StandardCharsets.UTF_8));
    }

    @Test
    public void testProcessLookUpCapabilitiesTaskWithPluginUnknown() {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("local")).when(bus).getBusIdentifier();
        PluginManager pluginManager = mock(PluginManager.class);
        BridgeConnectorManager bridgeConnectorManager = mock(BridgeConnectorManager.class);

        LookUpPlugin lookUpPlugin = new LookUpPlugin(pluginManager, bridgeConnectorManager, false);
        lookUpPlugin.setBus(bus);

        Request request = new Request(new GlobalIdentifier("local:a:b"),
                new GlobalIdentifier("local:commBridge:lookUp"),
                LookUpPlugin.lookUpCapabilitiesTask,
                new PluginIdentifier("abc:123").getBytes());

        lookUpPlugin.disallowExternalQueries();
        lookUpPlugin.processRequest(request);
        ArgumentCaptor<Response> responseCaptor = ArgumentCaptor.forClass(Response.class);
        verify(bus, never()).sendMessage(responseCaptor.capture(), any());
    }

    @Test
    public void testConvenienceMethods() {
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>(2);
        busIdentifiers.add(new BusIdentifier("abc"));
        busIdentifiers.add(new BusIdentifier("def"));
        assertEquals(busIdentifiers, LookUpPlugin.parseResponseForLookUpKnownExternalBusesTask("abc,def".getBytes(StandardCharsets.UTF_8)));

        ArrayList<PluginIdentifier> pluginIdentifiers = new ArrayList<>(2);
        pluginIdentifiers.add(new PluginIdentifier("abc:123"));
        pluginIdentifiers.add(new PluginIdentifier("def:456"));
        assertEquals(pluginIdentifiers, LookUpPlugin.parseResponseForLookUpPluginsTask("abc:123,def:456".getBytes(StandardCharsets.UTF_8)));

        ArrayList<Task> capabilities = new ArrayList<>(2);
        capabilities.add(new Task("abc"));
        capabilities.add(new Task("def"));
        assertEquals(capabilities, LookUpPlugin.parseResponseForLookUpCapabilitiesTask("abc,def".getBytes(StandardCharsets.UTF_8)));

        assertEquals(pluginIdentifiers, LookUpPlugin.parseResponseForLookUpPluginsWithCapabilityTask("abc:123,def:456".getBytes(StandardCharsets.UTF_8)));
    }
}
