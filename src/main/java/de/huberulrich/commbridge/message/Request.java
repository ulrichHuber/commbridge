/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.message;

import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.plugin.Task;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

/**
 * A message that can be sent via the Bus
 */
public class Request extends Message implements DataMessage {
    /**
     * MessageTypeByte of Request
     */
    public final static byte TypeByte = 0;

    /**
     * The maximal data size
     */
    private final static int mDataSize = 20480;

    /**
     * The task the recipient of the message shall perform
     */
    private final Task mTask;

    /**
     * The data needed for the task to be performed
     */
    private final byte[] mData;

    /**
     * A message that can be sent to plugins
     *
     * @param senderId    the global identifier of the sender
     * @param recipientId the global identifier of the recipient
     * @param task        the task, the recipient shall perform
     * @param data        the data needed for the task to be performed
     */
    public Request(GlobalIdentifier senderId, GlobalIdentifier recipientId, Task task, byte[] data) {
        super(senderId, recipientId);

        if (task == null)
            throw new IllegalArgumentException("Task cannot be null");

        mTask = task;

        if (data != null && data.length > mDataSize) {
            log.error("Size of data exceeds limit");
            throw new IllegalArgumentException("Data length exceeds maximum message size. The data this Request can " +
                    "hold must be lower or equal to " + mDataSize + "bytes");
        }

        mData = data;

        log.debug("\tMessageType: Request\n" +
                "\tTask: " + task.toString());
    }

    /**
     * A message that can be sent to plugins
     *
     * @param senderId    the global identifier of the sender
     * @param recipientId the global identifier of the recipient
     * @param messageId   the identifier of the message
     * @param task        the task, the recipient shall perform
     * @param data        the data needed for the task to be performed
     */
    private Request(GlobalIdentifier senderId, GlobalIdentifier recipientId, UUID messageId, Task task, byte[] data) {
        super(senderId, recipientId, messageId);

        mTask = task;
        mData = data;

        log.debug("\tMessageType: Request\n" +
                "\tTask: " + task.toString());
    }

    @Override
    public int getMessageLength() {
        return 1 + Character.BYTES + super.getMessageLength()
                + 1 + mTask.toString().getBytes(StandardCharsets.UTF_8).length
                + ((mData != null) ? mData.length : 0);
    }

    @Override
    public Request cloneWithNewBusIdentifier(BusIdentifier busIdentifier) throws CloneNotSupportedException {
        log.debug("Cloning Request for recipient bus: " + busIdentifier.toString());
        if (getRecipientIdentifier().getBusIdentifier().isExternalBroadcast() || getRecipientIdentifier().getBusIdentifier().isGlobalBroadcast())
            return new Request(getSenderIdentifier(), new GlobalIdentifier(busIdentifier, getRecipientIdentifier().getPluginIdentifier()), getMessageId(), mTask, mData);
        else {
            log.error("Tried to clone Request that is no broadcast");
            log.trace(toString());
            throw new CloneNotSupportedException("This Request cannot be cloned");
        }
    }

    /**
     * Get the task that shall be performed
     *
     * @return the task
     */
    public Task getTask() {
        return mTask;
    }

    /**
     * Get the data
     *
     * @return the data
     */
    public byte[] getData() {
        return mData;
    }

    @Override
    public byte[] getBytes() {
        byte[] messageBytes = super.getBytes();
        byte[] taskBytes = mTask.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(getMessageLength());
        byteBuffer.put(TypeByte);
        byteBuffer.putChar((char) messageBytes.length);
        byteBuffer.put(messageBytes);
        byteBuffer.put((byte) taskBytes.length);
        byteBuffer.put(taskBytes);
        if (mData != null)
            byteBuffer.put(mData);

        return byteBuffer.array();
    }

    /**
     * Restores a Message from the array of bytes generated by {@link #getBytes()}
     *
     * @param bytes the array of bytes
     * @return the restored Message
     */
    public static Request restoreFromBytes(byte[] bytes) throws ParseMessageException {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

        try {
            if (byteBuffer.get() != Request.TypeByte) {
                throw new ParseMessageException();
            }

            int messageBytesLength = byteBuffer.getChar();
            byte[] messageBytes = new byte[messageBytesLength];
            byteBuffer.get(messageBytes, 0, messageBytesLength);
            MessageDataHolder messageDataHolder = Message.getMessageDataFromBytes(messageBytes);

            byte taskBytesLength = byteBuffer.get();
            byte[] taskBytes = new byte[taskBytesLength];
            byteBuffer.get(taskBytes, 0, taskBytesLength);
            Task task = Task.restoreFromBytes(taskBytes);

            byte[] data = new byte[byteBuffer.remaining()];
            byteBuffer.get(data, 0, byteBuffer.remaining());

            return new Request(messageDataHolder.mSenderId, messageDataHolder.mRecipientId, messageDataHolder.mMessageId, task, data);
        } catch (BufferUnderflowException e) {
            throw new ParseMessageException();
        }
    }


    @Override
    public String toString() {
        return super.toString().concat("\n" +
                "\tType: Request\n" +
                "\tTask: " + mTask.toString());
    }
}
