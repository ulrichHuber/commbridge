/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import de.huberulrich.commbridge.identifier.BusIdentifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import javax.net.SocketFactory;
import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TcpBridgeConnectorTest {
    @Test
    public void testIncomingMessage() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();
        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage())));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5000);
        OutputStream outputStream = socket.getOutputStream();

        ByteBuffer sendBuffer = ByteBuffer.allocate(4);
        sendBuffer.putInt(10);
        outputStream.write(sendBuffer.array());

        byte[] data = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        outputStream.write(data);
        outputStream.flush();

        verify(manager, Mockito.timeout(10000)).sendMessageToBus(eq(data), any());
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testIncomingChunkedMessage() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();
        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage())));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5000);
        OutputStream outputStream = socket.getOutputStream();

        ByteBuffer sendBuffer = ByteBuffer.allocate(4);
        sendBuffer.putInt(9000);
        outputStream.write(sendBuffer.array());

        byte[] data = new byte[9000];
        for (int i = 0; i < 9000; i++)
            data[i] = (byte) (i % 128);

        outputStream.write(data);
        outputStream.flush();

        verify(manager, Mockito.timeout(10000)).sendMessageToBus(eq(data), any());
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testIncomingMessageBiggerThanPromised() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage())));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5000);
        OutputStream outputStream = socket.getOutputStream();

        ByteBuffer sendBuffer = ByteBuffer.allocate(4);
        sendBuffer.putInt(10);
        outputStream.write(sendBuffer.array());

        byte[] data = new byte[11];
        for (int i = 0; i < 11; i++)
            data[i] = (byte) (i % 128);

        outputStream.write(data);
        outputStream.flush();

        byte[] input = new byte[32];
        socket.getInputStream().read(input);

        assertArrayEquals("Message exceeded promised length".getBytes(StandardCharsets.UTF_8), input);
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testOutgoingMessage() throws Throwable {

        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();

        //OutputStream for Messages
        OutputStream outputStream = mock(OutputStream.class);
        ArgumentCaptor<byte[]> valueCapture = ArgumentCaptor.forClass(byte[].class);
        doNothing().when(outputStream).write(valueCapture.capture());

        Socket socket = mock(Socket.class);
        doReturn(outputStream).when(socket).getOutputStream();

        //OutputStream for Broadcasts
        OutputStream outputStreamBC = mock(OutputStream.class);

        Socket socketBC = mock(Socket.class);
        doReturn(outputStreamBC).when(socketBC).getOutputStream();

        SocketFactory socketFactory = mock(SocketFactory.class);
        doReturn(socketBC).when(socketFactory).createSocket((String) any(), anyInt());
        doReturn(socket).when(socketFactory).createSocket((InetAddress) any(), anyInt());

        TcpBridgeConnector tcpBridgeConnector = new TcpBridgeConnector(5000, e -> fail(e.getMessage()), socketFactory);
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        //register new host
        Socket socketAddHost = new Socket(InetAddress.getLoopbackAddress(), 5001);
        OutputStream outputStreamAddHost = socketAddHost.getOutputStream();
        outputStreamAddHost.write(new BusIdentifier("def456").toString().getBytes(StandardCharsets.UTF_8));
        outputStreamAddHost.flush();

        //wait for Connector to have the known Buses updated
        verify(manager, Mockito.timeout(10000)).bridgeConnectionStatusChanged(eq(tcpBridgeConnector));

        tcpBridgeConnector.outgoingMessage(new BusIdentifier("def456"), UUID.randomUUID(), new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});

        //Check the last two outgoing messages for the actual interesting data.
        assertArrayEquals(new byte[]{0, 0, 0, 10}, valueCapture.getAllValues().get(0));
        assertArrayEquals(new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0}, valueCapture.getAllValues().get(1));
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testOutgoingMessageToUnknownHost() throws Throwable {

        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();

        //OutputStream for Broadcasts
        OutputStream outputStreamBC = mock(OutputStream.class);

        Socket socketBC = mock(Socket.class);
        doReturn(outputStreamBC).when(socketBC).getOutputStream();

        SocketFactory socketFactory = mock(SocketFactory.class);
        doReturn(socketBC).when(socketFactory).createSocket((String) any(), anyInt());
        doThrow(UnknownHostException.class).when(socketFactory).createSocket((InetAddress) any(), anyInt());

        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage()), socketFactory));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        //register new host
        Socket socketAddHost = new Socket(InetAddress.getLoopbackAddress(), 5001);
        OutputStream outputStreamAddHost = socketAddHost.getOutputStream();
        outputStreamAddHost.write(new BusIdentifier("def456").toString().getBytes(StandardCharsets.UTF_8));
        outputStreamAddHost.flush();

        //wait for Connector to have the known Buses updated
        verify(manager, Mockito.timeout(10000)).bridgeConnectionStatusChanged(eq(tcpBridgeConnector));

        tcpBridgeConnector.outgoingMessage(new BusIdentifier("def456"), UUID.randomUUID(), new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 0});

        //Check the last two outgoing messages for the actual interesting data.
        verify(tcpBridgeConnector, Mockito.timeout(10000)).incomingMessage(any(), any());

        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testIncomingIdentifier() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();
        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage())));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5001);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(new BusIdentifier("def456").toString().getBytes(StandardCharsets.UTF_8));
        outputStream.flush();

        verify(manager, Mockito.timeout(10000)).bridgeConnectionStatusChanged(eq(tcpBridgeConnector));
        ArrayList<BusIdentifier> identifiers = new ArrayList<>(1);
        identifiers.add(new BusIdentifier("def456"));
        assertEquals(identifiers, tcpBridgeConnector.getConnectedBusIdentifiers());
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testIncomingIdentifierWithMaximumLength() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        doReturn(new BusIdentifier("abc123")).when(manager).getConnectedBusIdentifier();
        TcpBridgeConnector tcpBridgeConnector = spy(new TcpBridgeConnector(5000, e -> fail(e.getMessage())));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5001);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write(new BusIdentifier("abcdefghijklmnopqrstuvwxyzabcdef").toString().getBytes(StandardCharsets.UTF_8));
        outputStream.flush();

        verify(manager, Mockito.timeout(10000)).bridgeConnectionStatusChanged(eq(tcpBridgeConnector));
        ArrayList<BusIdentifier> identifiers = new ArrayList<>(1);
        identifiers.add(new BusIdentifier("abcdefghijklmnopqrstuvwxyzabcdef"));
        assertEquals(identifiers, tcpBridgeConnector.getConnectedBusIdentifiers());
        tcpBridgeConnector.closeListeners();
    }

    @Test
    public void testIncomingIdentifierWithTooLong() throws IOException {
        BridgeConnectorManager manager = mock(BridgeConnectorManager.class);
        TcpBridgeConnector tcpBridgeConnector = new TcpBridgeConnector(5000, e -> fail(e.getMessage()));
        tcpBridgeConnector.setBridgeConnectorManager(manager);

        Socket socket = new Socket(InetAddress.getLoopbackAddress(), 5001);
        OutputStream outputStream = socket.getOutputStream();
        outputStream.write("abcdefghijklmnopqrstuvwxyzabcdefg".getBytes(StandardCharsets.UTF_8));
        outputStream.flush();

        byte[] input = new byte[34];
        socket.getInputStream().read(input);

        assertArrayEquals("Identifier exceeded maximum length".getBytes(StandardCharsets.UTF_8), input);
        tcpBridgeConnector.closeListeners();
    }
}
