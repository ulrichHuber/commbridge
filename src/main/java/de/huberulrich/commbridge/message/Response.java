/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.message;

import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.UUID;

public class Response extends Message implements ResponseMessage, DataMessage {
    /**
     * MessageTypeByte of Response
     */
    public final static byte TypeByte = 1;

    /**
     * The maximal data size
     */
    private final static int mDataSize = 20480;

    /**
     * The id of the message this Acknowledgement is for.
     */
    private final UUID mOriginalMessageId;

    /**
     * The data needed for the task to be performed
     */
    private final byte[] mData;

    /**
     * A message that can be sent to plugins
     *
     * @param senderId          the global identifier of the sender
     * @param recipientId       the global identifier of the recipient
     * @param originalMessageId the id of the message this is the response to
     * @param data              the data needed for the task to be performed
     */
    public Response(GlobalIdentifier senderId, GlobalIdentifier recipientId, UUID originalMessageId, byte[] data) {
        super(senderId, recipientId);

        if (originalMessageId == null)
            throw new IllegalArgumentException("The id of the original message cannot be null");

        log.debug("New Message is Response for message " + originalMessageId.toString());

        if (recipientId.getBusIdentifier().isExternalBroadcast() || recipientId.getBusIdentifier().isGlobalBroadcast() || recipientId.getPluginIdentifier().isBroadcast())
            throw new IllegalArgumentException("The recipient of a Response has to be specified completely");

        if (data != null && data.length > mDataSize) {
            log.error("Size of data exceeds limit");
            throw new IllegalArgumentException("Data length exceeds maximum message size. The data this Response can " +
                    "hold must be lower or equal to " + mDataSize + "bytes");
        }

        mOriginalMessageId = originalMessageId;
        mData = data;

        log.trace(toString());
    }

    /**
     * A message that can be sent to plugins
     *
     * @param senderId          the global identifier of the sender
     * @param recipientId       the global identifier of the recipient
     * @param messageId         the identifier of the message
     * @param originalMessageId the id of the message this is the response to
     * @param data              the data needed for the task to be performed
     */
    private Response(GlobalIdentifier senderId, GlobalIdentifier recipientId, UUID messageId, UUID originalMessageId, byte[] data) {
        super(senderId, recipientId, messageId);

        log.debug("New Message is Response");

        mOriginalMessageId = originalMessageId;
        mData = data;

        log.trace(toString());
    }

    @Override
    public int getMessageLength() {
        return 1 //TypeByte
                + Character.BYTES + super.getMessageLength() //Bytes of parent
                + 2 * Long.BYTES //mOriginalMessageId
                + ((mData != null) ? mData.length : 0); //Data
    }

    @Override
    public Response cloneWithNewBusIdentifier(BusIdentifier busIdentifier) throws CloneNotSupportedException {
        throw new CloneNotSupportedException("A Response cannot be cloned");
    }

    /**
     * Get the id of the message this is the response to
     *
     * @return the id of the message this is the response to
     */
    @Override
    public UUID getOriginalMessageId() {
        return mOriginalMessageId;
    }

    /**
     * Get the data
     *
     * @return the data
     */
    public byte[] getData() {
        return mData;
    }

    @Override
    public byte[] getBytes() {
        byte[] messageBytes = super.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.allocate(getMessageLength());
        byteBuffer.put(TypeByte);
        byteBuffer.putChar((char) messageBytes.length);
        byteBuffer.put(messageBytes);
        byteBuffer.putLong(mOriginalMessageId.getMostSignificantBits());
        byteBuffer.putLong(mOriginalMessageId.getLeastSignificantBits());
        if (mData != null)
            byteBuffer.put(mData);

        return byteBuffer.array();
    }

    /**
     * Restores a Message from the array of bytes generated by {@link #getBytes()}
     *
     * @param bytes the array of bytes
     * @return the restored Message
     */
    public static Response restoreFromBytes(byte[] bytes) throws ParseMessageException {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

        try {
            if (byteBuffer.get() != Response.TypeByte) {
                throw new ParseMessageException();
            }

            int messageBytesLength = byteBuffer.getChar();
            byte[] messageBytes = new byte[messageBytesLength];
            byteBuffer.get(messageBytes, 0, messageBytesLength);
            MessageDataHolder messageDataHolder = Message.getMessageDataFromBytes(messageBytes);

            long mostSignificantBits = byteBuffer.getLong();
            long leastSignificantBits = byteBuffer.getLong();
            UUID originalMessageId = new UUID(mostSignificantBits, leastSignificantBits);

            byte[] data = new byte[byteBuffer.remaining()];
            byteBuffer.get(data, 0, byteBuffer.remaining());

            return new Response(messageDataHolder.mSenderId, messageDataHolder.mRecipientId,
                    messageDataHolder.mMessageId, originalMessageId, data);
        } catch (BufferUnderflowException e) {
            throw new ParseMessageException();
        }
    }


    @Override
    public String toString() {
        return super.toString().concat("\n" +
                "\tType: Response\n" +
                "\tOriginalMessageId: " + mOriginalMessageId);
    }
}
