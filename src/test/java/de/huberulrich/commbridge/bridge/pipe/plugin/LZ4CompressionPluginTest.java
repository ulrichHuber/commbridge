/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.exceptions.PipeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Random;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.fail;

@RunWith(MockitoJUnitRunner.class)
public class LZ4CompressionPluginTest {
    private LZ4CompressionPlugin lz4CompressionPlugin;
    private byte[] messageBytes;

    public LZ4CompressionPluginTest() {
        Random random = new Random(Generators.timeBasedGenerator().generate().getMostSignificantBits());
        lz4CompressionPlugin = new LZ4CompressionPlugin();
        messageBytes = new byte[400];
        random.nextBytes(messageBytes);
    }

    @Test
    public void testApplyAndUndoAreMatchingOperations() {
        try {
            assertArrayEquals(messageBytes, lz4CompressionPlugin.undo(lz4CompressionPlugin.apply(messageBytes)));
        } catch (PipeException e) {
            fail();
        }
    }
}
