/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.message;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;
import java.util.UUID;

public abstract class Message {
    final Log log = LogFactory.getLog(Message.class);
    /**
     * The identifier of the sender of this message
     */
    private final GlobalIdentifier mSenderId;

    /**
     * The identifier of the recipient of this message
     */
    private final GlobalIdentifier mRecipientId;

    /**
     * The id of this message.
     */
    private final UUID mMessageId;

    /**
     * A message that can be sent to plugins
     *
     * @param senderId    the global identifier of the sender
     * @param recipientId the global identifier of the recipient
     */
    Message(GlobalIdentifier senderId, GlobalIdentifier recipientId) {
        if (senderId == null || recipientId == null)
            throw new IllegalArgumentException("Ids cannot be null");
        if (senderId.getBusIdentifier().isExternalBroadcast() || senderId.getBusIdentifier().isGlobalBroadcast() || senderId.getPluginIdentifier().isBroadcast())
            throw new IllegalArgumentException("The sender of a Message has to be specified completely");

        mSenderId = senderId;
        mRecipientId = recipientId;
        mMessageId = Generators.timeBasedGenerator().generate();

        log.debug("Creating new Message:" +
                "\tSenderId: " + senderId.toString() + "\n" +
                "\tRecipientId: " + recipientId.toString() + "\n" +
                "\tMessageId: " + mMessageId.toString() + "\n");
    }

    /**
     * A message that can be sent to plugins
     *
     * @param senderId    the global identifier of the sender
     * @param recipientId the global identifier of the recipient
     * @param messageId   the identifier of the message
     */
    Message(GlobalIdentifier senderId, GlobalIdentifier recipientId, UUID messageId) {
        mSenderId = senderId;
        mRecipientId = recipientId;
        mMessageId = messageId;

        log.debug("Creating new Message:" +
                "\tSenderId: " + senderId.toString() + "\n" +
                "\tRecipientId: " + recipientId.toString() + "\n" +
                "\tMessageId: " + messageId.toString() + "\n");
    }

    /**
     * Get the identifier of the sender
     *
     * @return the identifier
     */
    public final GlobalIdentifier getSenderIdentifier() {
        return mSenderId;
    }

    /**
     * Get the identifier of the recipient
     *
     * @return the identifier
     */
    public final GlobalIdentifier getRecipientIdentifier() {
        return mRecipientId;
    }

    /**
     * Get the id of this message
     *
     * @return the id
     */
    public final UUID getMessageId() {
        return mMessageId;
    }

    private int getInternalMessageLength() {
        return 16 + //UUID
                1 + mSenderId.getBytes().length + //SenderId
                1 + mRecipientId.getBytes().length; //RecipientId
    }

    /**
     * Get the length of the data in bytes
     *
     * @return the length of the data in bytes
     */
    public int getMessageLength() {
        return getInternalMessageLength();
    }

    /**
     * Clones the Message with a specified new recipient bus, if the Message was previously a broadcast.
     *
     * @param busIdentifier the new busIdentifier
     */
    public abstract Message cloneWithNewBusIdentifier(BusIdentifier busIdentifier) throws CloneNotSupportedException;

    /**
     * Generates an array of bytes from which the original message can be restored.
     *
     * @return the array of bytes that can be parsed to the message
     */
    public byte[] getBytes() {

        ByteBuffer byteBuffer = ByteBuffer.allocate(getInternalMessageLength());
        byteBuffer.putLong(mMessageId.getMostSignificantBits());
        byteBuffer.putLong(mMessageId.getLeastSignificantBits());

        byte[] senderIdBytes = mSenderId.getBytes();
        byteBuffer.put((byte) senderIdBytes.length);
        byteBuffer.put(senderIdBytes);

        byte[] recipientIdBytes = mRecipientId.getBytes();
        byteBuffer.put((byte) recipientIdBytes.length);
        byteBuffer.put(recipientIdBytes);

        return byteBuffer.array();
    }

    static MessageDataHolder getMessageDataFromBytes(byte[] bytes) throws ParseMessageException {
        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);

        try {
            long mostSignificantBits = byteBuffer.getLong();
            long leastSignificantBits = byteBuffer.getLong();
            UUID messageId = new UUID(mostSignificantBits, leastSignificantBits);

            byte senderIdLength = byteBuffer.get();
            byte[] senderIdBytes = new byte[senderIdLength];
            byteBuffer.get(senderIdBytes, 0, senderIdLength);
            GlobalIdentifier senderId = GlobalIdentifier.restoreFromBytes(senderIdBytes);

            byte recipientIdLength = byteBuffer.get();
            byte[] recipientIdBytes = new byte[recipientIdLength];
            byteBuffer.get(recipientIdBytes, 0, recipientIdLength);
            GlobalIdentifier recipientId = GlobalIdentifier.restoreFromBytes(recipientIdBytes);

            return new MessageDataHolder(senderId, recipientId, messageId);
        } catch (BufferUnderflowException e) {
            throw new ParseMessageException();
        }
    }

    @Override
    public String toString() {
        return "Message\n" +
                "\tSender: " + mSenderId.toString() + "\n" +
                "\tRecipient: " + mRecipientId.toString() + "\n" +
                "\tID: " + mMessageId.toString();
    }

    @Override
    public int hashCode() {
        //Message ids are time-based UUIDs and therefore they should be reliably diverse for hash codes
        return getMessageId().hashCode();
    }

    static class MessageDataHolder {
        final GlobalIdentifier mSenderId;
        final GlobalIdentifier mRecipientId;
        final UUID mMessageId;

        MessageDataHolder(GlobalIdentifier senderId, GlobalIdentifier recipientId, UUID messageId) {
            mSenderId = senderId;
            mRecipientId = recipientId;
            mMessageId = messageId;
        }
    }
}
