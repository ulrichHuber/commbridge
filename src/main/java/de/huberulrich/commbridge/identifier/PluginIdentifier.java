/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.identifier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.StandardCharsets;

/**
 * Identifier for a Plugin
 */
public class PluginIdentifier {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(PluginIdentifier.class);

    /**
     * The actual identifier
     */
    private final String mPluginIdentifier;

    /**
     * Constructs a new plugin-identifier
     *
     * @param identifier the identifier with only:
     *                   - alphanumeric characters
     *                   - or "*" for a local broadcast (all plugins)
     */
    @SuppressWarnings("WeakerAccess")
    public PluginIdentifier(String identifier) {
        if (identifier == null || !identifier.matches("^(?:[A-Za-z0-9]+:[A-Za-z0-9]+|\\*)$")) {
            log.error("Invalid plugin-identifier: " + identifier);
            throw new IllegalArgumentException("The provided identifier is in an invalid format");
        }

        if (identifier.length() > 65) {
            log.error("New identifier exceeds maximal length: " + identifier);
            throw new IllegalArgumentException("The provided identifier exceeds the maximal length of 65");
        }

        mPluginIdentifier = identifier;
    }

    /**
     * Whether the identifier matches all plugins
     *
     * @return Whether the identifier is a broadcast
     */
    public boolean isBroadcast() {
        return !mPluginIdentifier.matches("^[A-Za-z]+:[A-Za-z]+$");
    }

    /**
     * Gets the byte array representation of this global-identifier
     *
     * @return the byte representation
     */
    public byte[] getBytes() {
        return toString().getBytes(StandardCharsets.UTF_8);
    }

    /**
     * Restores a GlobalIdentifier from the array of bytes generated by {@link #getBytes()}
     *
     * @param bytes the array with bytes
     * @return the restored GlobalIdentifier
     */
    public static PluginIdentifier restoreFromBytes(byte[] bytes) {
        return new PluginIdentifier(new String(bytes, StandardCharsets.UTF_8));
    }

    @Override
    public String toString() {
        return mPluginIdentifier;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof PluginIdentifier && obj.toString().equals(toString());
    }

    @Override
    public int hashCode() {
        return mPluginIdentifier.hashCode();
    }
}
