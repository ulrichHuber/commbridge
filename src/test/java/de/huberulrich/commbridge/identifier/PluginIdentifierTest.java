/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.identifier;

import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class PluginIdentifierTest {
    @Test
    public void testValidIdentifier() {
        new PluginIdentifier("123:456");
        new PluginIdentifier("abc123:def456");
        new PluginIdentifier("ABCabc123:DEFdef456");
        new PluginIdentifier("*");
    }

    @Test
    public void testMaximumIdentifierLength() {
        new PluginIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ghijklmnopqrstuvwxyz1234567890AB");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyIdentifier() {
        new PluginIdentifier("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptySubStringIdentifier() {
        new PluginIdentifier(":");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier() {
        new PluginIdentifier(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDisallowedCharacterIdentifier() {
        new PluginIdentifier(".");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooLongIdentifier() {
        new PluginIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ghijklmnopqrstuvwxyz1234567890ABC");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLineBreakIdentifier() {
        new PluginIdentifier("a:b\n");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidWithWhitespaceAtEndIdentifier() {
        new PluginIdentifier("abc: ");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidWithWhitespaceAtBeginningIdentifier() {
        new PluginIdentifier(" :abc");
    }

    @Test
    public void testIsBroadcast() {
        assertTrue(new PluginIdentifier("*").isBroadcast());
        assertFalse(new PluginIdentifier("abc:def").isBroadcast());
    }

    @Test
    public void testGetBytes() {
        assertArrayEquals("abc:def".getBytes(StandardCharsets.UTF_8), new PluginIdentifier("abc:def").getBytes());
    }

    @Test
    public void testRestoreFromBytes() {
        assertEquals(new PluginIdentifier("abc:def"), PluginIdentifier.restoreFromBytes("abc:def".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    public void testToString() {
        assertEquals("abc:def", new PluginIdentifier("abc:def").toString());
    }

    @Test
    public void testEquals() {
        assertEquals(new PluginIdentifier("abc:def"), new PluginIdentifier("abc:def"));
        assertNotEquals(new PluginIdentifier("abc:def"), new PluginIdentifier("def:ghi"));
    }

    @Test
    public void testRepeatableHashCode() {
        assertEquals(new PluginIdentifier("abc:def").hashCode(), new PluginIdentifier("abc:def").hashCode());
        assertNotEquals(new PluginIdentifier("abc:def").hashCode(), new PluginIdentifier("def:ghi").hashCode());
    }
}
