/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge;

import de.huberulrich.commbridge.bridge.BridgeConnector;
import de.huberulrich.commbridge.bridge.BridgeConnectorPriority;
import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Acknowledgement;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.Response;
import de.huberulrich.commbridge.message.ResponseListener;
import de.huberulrich.commbridge.plugin.Plugin;
import de.huberulrich.commbridge.plugin.Task;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.UUID;

import static org.junit.Assert.*;

public class IntegrationTest {
    private boolean acknowledgementReceived = false;
    private boolean responseReceived = false;

    @Before
    public void setUp() {
        acknowledgementReceived = false;
        responseReceived = false;
    }

    @Test
    public void testEchoMessageWithFullySpecifiedIdentifier() {
        Bus bus = new Bus(new BusIdentifier("myTestBus"));
        bus.getPluginManager().registerPlugin(new EchoPlugin());
        TestPlugin testPlugin = new TestPlugin();
        bus.getPluginManager().registerPlugin(testPlugin);
        try {
            testPlugin.sendTask(new GlobalIdentifier("myTestBus:test:echoPlugin"));
        } catch (NotRegisteredException e) {
            fail();
        }

        assertTrue(acknowledgementReceived);
        assertTrue(responseReceived);
    }

    @Test
    public void testEchoMessageWithWhitespaceIdentifier() {
        Bus bus = new Bus(new BusIdentifier("myTestBus"));
        bus.getPluginManager().registerPlugin(new EchoPlugin());
        TestPlugin testPlugin = new TestPlugin();
        bus.getPluginManager().registerPlugin(testPlugin);
        try {
            testPlugin.sendTask(new GlobalIdentifier("myTestBus:*"));
        } catch (NotRegisteredException e) {
            fail();
        }

        assertTrue(acknowledgementReceived);
        assertTrue(responseReceived);
    }

    @Test
    public void testEchoMessageOverBridgeConnector() throws NotRegisteredException {
        Bus echoBus = new Bus(new BusIdentifier("myEchoBus"));
        echoBus.getPluginManager().registerPlugin(new EchoPlugin());

        Bus testBus = new Bus(new BusIdentifier("myTestBus"));
        TestPlugin testPlugin = new TestPlugin();
        testBus.getPluginManager().registerPlugin(testPlugin);

        DirectBridgeConnector bus1Connector = new DirectBridgeConnector();
        echoBus.getBridgeConnectorManager().registerBridgeConnector(bus1Connector);

        DirectBridgeConnector bus2Connector = new DirectBridgeConnector();
        testBus.getBridgeConnectorManager().registerBridgeConnector(bus2Connector);

        bus1Connector.connect(bus2Connector);
        bus2Connector.connect(bus1Connector);

        try {
            testPlugin.sendTask(new GlobalIdentifier("*:*"));
        } catch (NotRegisteredException e) {
            fail();
        }

        assertTrue(acknowledgementReceived);
        assertTrue(responseReceived);
    }

    class TestPlugin extends Plugin {

        @Override
        public PluginIdentifier getPluginIdentifier() {
            return new PluginIdentifier("test:testPlugin");
        }

        @Override
        public String getUserReadableName() {
            return "TestPlugin";
        }

        @Override
        public Task[] getCapabilities() {
            return new Task[]{};
        }

        @Override
        public void processRequest(Request message) {

        }

        void sendTask(GlobalIdentifier recipientId) throws NotRegisteredException {
            ByteBuffer byteBuffer = ByteBuffer.allocate(8);
            byteBuffer.putInt(1);
            byteBuffer.putInt(2);
            pushRequestToBus(recipientId, new Task("echo"), byteBuffer.array(), e -> fail(), new ResponseListener() {
                @Override
                public void onAcknowledgementReceived(Acknowledgement acknowledgement) {
                    acknowledgementReceived = true;
                }

                @Override
                public void onResponseReceived(Response response) {
                    assertArrayEquals(byteBuffer.array(), response.getData());
                    responseReceived = true;
                }

                @Override
                public void onTimeout() {
                    fail();
                }
            }, 1000);
        }
    }

    class EchoPlugin extends Plugin {
        @Override
        public PluginIdentifier getPluginIdentifier() {
            return new PluginIdentifier("test:echoPlugin");
        }

        @Override
        public String getUserReadableName() {
            return "EchoPlugin";
        }

        @Override
        public Task[] getCapabilities() {
            return new Task[]{new Task("echo")};
        }

        @Override
        public void processRequest(Request message) {
            if (message.getTask().equals(new Task("echo"))) {
                try {
                    pushResponseToBus(message, message.getData(), e -> fail());
                } catch (NotRegisteredException e) {
                    fail();
                }
            }
        }
    }

    class DirectBridgeConnector extends BridgeConnector {
        private DirectBridgeConnector mBridgeConnector;

        void connect(DirectBridgeConnector bridgeConnector) throws NotRegisteredException {
            mBridgeConnector = bridgeConnector;
            ArrayList<BusIdentifier> identifiers = new ArrayList<>();
            identifiers.add(bridgeConnector.getLocalBusIdentifier());
            setConnectedBusIdentifiers(identifiers);
        }

        @Override
        public BridgeConnectorPriority getPriority(int dataLength) {
            return BridgeConnectorPriority.EFFICIENT_FAST;
        }

        @Override
        public String getReadableName() {
            return "DirectBridgeConnector";
        }

        @Override
        public void outgoingMessage(BusIdentifier recipient, UUID messageId, byte[] messageBytes) {
            if (mBridgeConnector != null) {
                try {
                    mBridgeConnector.incomingMessage(messageBytes);
                } catch (PipeException | NotRegisteredException e) {
                    e.printStackTrace();
                }
            }
        }

        void incomingMessage(byte[] messageBytes) throws PipeException, NotRegisteredException {
            incomingMessage(messageBytes, e -> fail());
        }
    }
}
