/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import java.util.Comparator;

/**
 * Comparator used by the BridgeConnectorManager to find the best suited bridge for a given message
 */
class BridgeConnectorPriorityComparator implements Comparator<BridgeConnector> {
    /**
     * The length of the data contained in the message
     */
    private final int mDataLength;

    /**
     * Constructs a new Comparator
     *
     * @param dataLength the length of the data contained in the message
     */
    BridgeConnectorPriorityComparator(int dataLength) {
        mDataLength = dataLength;
    }

    @Override
    public int compare(BridgeConnector o1, BridgeConnector o2) {
        int o1Priority = o1.getPriority(mDataLength).ordinal();
        int o2Priority = o2.getPriority(mDataLength).ordinal();

        return Integer.compare(o1Priority, o2Priority);
    }
}
