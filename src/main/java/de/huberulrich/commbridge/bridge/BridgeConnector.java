/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.bridge.pipe.MessagePipe;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.message.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ExceptionListener;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public abstract class BridgeConnector {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(BridgeConnector.class);

    /**
     * Used for internal hashCode generation
     */
    private final UUID uuid = Generators.timeBasedGenerator().generate();

    /**
     * The unique id
     */
    private ArrayList<BusIdentifier> mConnectedBusIdentifiers = new ArrayList<>();

    /**
     * Internal synchronization object for mConnectedBusIdentifiers
     */
    private ReadWriteLock mConnectedBusIdentifiersLock = new ReentrantReadWriteLock();

    /**
     * The manager this connector is registered to
     */
    private BridgeConnectorManager mBridgeConnectorManager = null;

    /**
     * Internal synchronization object for mBridgeConnectorManager
     */
    private ReadWriteLock mBridgeConnectorManagerLock = new ReentrantReadWriteLock();

    /**
     * The pipe to run Messages through, before sending them via a BridgeConnector
     */
    private MessagePipe mMessagePipe;

    /**
     * Internal synchronization object for mBridgeConnectorManager
     */
    private ReadWriteLock mMessagePipeLock = new ReentrantReadWriteLock();

    /**
     * Get the globally unique identifiers of the buses this bridge is connected to
     *
     * @return the identifiers
     */
    protected final ArrayList<BusIdentifier> getConnectedBusIdentifiers() {
        mConnectedBusIdentifiersLock.readLock().lock();
        try {
            return mConnectedBusIdentifiers;
        } finally {
            mConnectedBusIdentifiersLock.readLock().unlock();
        }
    }

    /**
     * Updates the list of buss connected to this bridge. This list is used by the manager to decide which bridge to
     * choose for transmission of messages. When connection to a bus is lost (even if only temporarily), the bus
     * should be removed from the list.
     *
     * @param connectedBusIdentifiers the new list of connected buses
     */
    protected final void setConnectedBusIdentifiers(ArrayList<BusIdentifier> connectedBusIdentifiers) {
        String logAppend;
        if (mBridgeConnectorManager != null)
            logAppend = "[Bus:" + mBridgeConnectorManager.getConnectedBusIdentifier() + "][BridgeConnector:" + getReadableName() + "] ";
        else
            logAppend = "[Bus: Not connected][BridgeConnector:" + getReadableName() + "] ";

        log.info(logAppend + "Changing list of connected buses");
        log.debug(logAppend + "Old list of connected buses:\n" + mConnectedBusIdentifiers);

        if (connectedBusIdentifiers == null) {
            log.error(logAppend + "Failed since NULL is not allowed as list of identifiers");
            throw new IllegalArgumentException("Set an empty ArrayList if no buses are connected and not NULL");
        }

        if (connectedBusIdentifiers.contains(new BusIdentifier("*"))
                || connectedBusIdentifiers.contains(new BusIdentifier("-"))) {
            log.error(logAppend + "Failed since broadcast identifiers are not allowed in the list of identifiers");
            throw new IllegalArgumentException("No broadcast identifiers allowed");
        }

        mConnectedBusIdentifiersLock.writeLock().lock();
        try {
            mConnectedBusIdentifiers = connectedBusIdentifiers;
        } finally {
            mConnectedBusIdentifiersLock.writeLock().unlock();
        }

        log.debug(logAppend + "New list of connected buses:\n" + mConnectedBusIdentifiers.toString());

        //notify the BridgeConnectorManager about this change in connection status, so he can reevaluate the available routes.
        mBridgeConnectorManagerLock.readLock().lock();
        try {
            if (mBridgeConnectorManager != null)
                mBridgeConnectorManager.bridgeConnectionStatusChanged(this);
        } finally {
            mBridgeConnectorManagerLock.readLock().unlock();
        }

        log.debug(logAppend + "Finished changing list of connected buses");
    }

    /**
     * Get the priority of this connector
     * <p>
     * When multiple bridges connect to the same bus, this decides which one is used to send a message. The best bridge
     * is selected on a per message basis. The lower the value the higher the priority. The best priority is 0.
     * <p>
     * For example a bridge with bad energy consumption should get a low priority. The priority can change over time,
     * for example when the bridge has a power saving feature under special circumstances.
     *
     * @param dataLength The length of the data that is sent via the message
     * @return the priority
     */
    public abstract BridgeConnectorPriority getPriority(int dataLength);

    /**
     * Get the readable name of this connector.
     *
     * @return the readable name
     */
    public abstract String getReadableName();

    /**
     * Set the BridgeConnectorManager this plugin is registered to
     *
     * @param bridgeConnectorManager the BridgeConnectorManager
     */
    final void setBridgeConnectorManager(BridgeConnectorManager bridgeConnectorManager) {
        if (bridgeConnectorManager != null && mBridgeConnectorManager != null)
            throw new IllegalArgumentException("You can't register a BridgeConnector to two Managers");

        mBridgeConnectorManagerLock.writeLock().lock();
        try {
            mBridgeConnectorManager = bridgeConnectorManager;
        } finally {
            mBridgeConnectorManagerLock.writeLock().unlock();
        }

        if (mBridgeConnectorManager != null)
            onRegisteredToManager();
    }

    /**
     * Called when this BridgeConnector is registered with a BridgeConnectorManager
     */
    protected void onRegisteredToManager() {

    }

    /**
     * Gets the BusIdentifier of the Bus this BridgeConnector is connected to via the BridgeConnectorManager
     *
     * @return the bus-identifier
     * @throws NotRegisteredException Thrown when BridgeConnector is not connected to a Bus
     */
    public final BusIdentifier getLocalBusIdentifier() throws NotRegisteredException {
        mBridgeConnectorManagerLock.readLock().lock();
        try {
            if (mBridgeConnectorManager != null)
                return mBridgeConnectorManager.getConnectedBusIdentifier();
            else
                throw new NotRegisteredException();
        } finally {
            mBridgeConnectorManagerLock.readLock().unlock();
        }
    }

    /**
     * Gets the MessagePipe this BridgeConnector uses.
     *
     * @param bridgeConnectorManager The BridgeConnectorManager this BridgeConnector is registered to. If not yet registered, use NULL.
     *                               Used to secure MessagePipe against tampering
     * @return the MessagePipe or null
     */
    final MessagePipe getMessagePipe(BridgeConnectorManager bridgeConnectorManager) throws IllegalAccessException {
        if (mBridgeConnectorManager != bridgeConnectorManager)
            throw new IllegalAccessException();

        mMessagePipeLock.readLock().lock();
        try {
            return mMessagePipe;
        } finally {
            mMessagePipeLock.readLock().unlock();
        }
    }

    /**
     * Sets the MessagePipe this BridgeConnector uses.
     *
     * @param bridgeConnectorManager The BridgeConnectorManager this BridgeConnector is registered to. If not yet registered, use NULL.
     *                               Used to secure MessagePipe against tampering
     * @param messagePipe            the MessagePipe
     */
    final void setMessagePipe(BridgeConnectorManager bridgeConnectorManager, MessagePipe messagePipe) throws IllegalAccessException {
        if (mBridgeConnectorManager != bridgeConnectorManager)
            throw new IllegalAccessException();

        mMessagePipeLock.writeLock().lock();
        try {
            this.mMessagePipe = messagePipe;
        } finally {
            mMessagePipeLock.writeLock().unlock();
        }
    }

    /**
     * Removes the MessagePipe this BridgeConnector uses.
     *
     * @param bridgeConnectorManager The BridgeConnectorManager this BridgeConnector is registered to. If not yet registered, use NULL.
     *                               Used to secure MessagePipe against tampering
     */
    final void removeMessagePipe(BridgeConnectorManager bridgeConnectorManager) throws IllegalAccessException {
        setMessagePipe(bridgeConnectorManager, null);
    }

    /**
     * Creates from the provided array of bytes a Message, which is pushed on the Bus. This message can also be relayed
     * to other buses via the bus (External delivery).
     *
     * @param messageBytes      the received data
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          ParseMessageException: Thrown when the Message could not be parsed
     *                          </li>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    protected final void incomingMessage(byte[] messageBytes, ExceptionListener exceptionListener)
            throws NotRegisteredException {
        mBridgeConnectorManagerLock.readLock().lock();
        try {
            String logAppend;
            if (mBridgeConnectorManager != null)
                logAppend = "[Bus:" + mBridgeConnectorManager.getConnectedBusIdentifier() + "][BridgeConnector:" + getReadableName() + "] ";
            else
                logAppend = "[Bus: Not connected][BridgeConnector:" + getReadableName() + "] ";

            log.debug(logAppend + "Pushing message to BridgeConnectorManager");
            if (mBridgeConnectorManager == null) {
                log.error(logAppend + "Trying to push message to bus while not being connected to a BridgeConnectorManager");
                throw new NotRegisteredException();
            }

            //run message through pipe
            mMessagePipeLock.readLock().lock();
            try {
                if (mMessagePipe != null)
                    try {
                        messageBytes = mMessagePipe.undoPipe(messageBytes);
                    } catch (Exception e) {
                        log.error(logAppend + "Exception occurred while undoing MessagePipe");
                        if (exceptionListener != null)
                            exceptionListener.exceptionThrown(e);
                    }
            } finally {
                mMessagePipeLock.readLock().unlock();
            }

            mBridgeConnectorManager.sendMessageToBus(messageBytes, exceptionListener);

            log.debug(logAppend + "Finished pushing message to BridgeConnectorManager");
        } finally {
            mBridgeConnectorManagerLock.readLock().unlock();
        }
    }

    /**
     * Method called by the BridgeConnectorManager to push messages through this BridgeConnector. This method pushes the
     * message through the MessagePipe if one exists, prior to letting the implementation handle transmission
     *
     * @param message The message
     * @throws PipeException Thrown if an exception within the Pipe occurred
     */
    final void pushThroughPipeToOutgoing(Message message) throws PipeException {
        if (message == null)
            throw new IllegalArgumentException();

        //run message through pipe
        byte[] messageBytes = message.getBytes();
        mMessagePipeLock.readLock().lock();
        try {
            if (mMessagePipe != null)
                messageBytes = mMessagePipe.applyPipe(messageBytes);
        } finally {
            mMessagePipeLock.readLock().unlock();
        }
        outgoingMessage(message.getRecipientIdentifier().getBusIdentifier(), message.getMessageId(), messageBytes);
    }

    /**
     * Handles outgoing messages that need to be sent via this bridge.
     * Please take care of concurrency and run long tasks in a separate thread.
     *
     * @param recipient    the BusIdentifier of the recipient
     * @param messageId    the unique id of the message (Compare messageIds with UUIDComparator.staticCompare(m1, m2))
     * @param messageBytes the message as an array of bytes
     */
    protected abstract void outgoingMessage(BusIdentifier recipient, UUID messageId, byte[] messageBytes);

    @Override
    public final int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return "BridgeConnector\n" +
                "\tConnected buses: " + mConnectedBusIdentifiers.toString();
    }
}
