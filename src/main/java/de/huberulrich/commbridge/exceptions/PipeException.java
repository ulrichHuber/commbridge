/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.exceptions;

import de.huberulrich.commbridge.bridge.pipe.plugin.PipePlugin;

public class PipeException extends Exception {
    private final PipePlugin mPipePlugin;

    public PipeException(PipePlugin pipePlugin, String message) {
        super(message);
        mPipePlugin = pipePlugin;
    }

    public PipeException(PipePlugin pipePlugin, String message, Throwable throwable) {
        super(message, throwable);
        mPipePlugin = pipePlugin;
    }

    public PipePlugin getPipePlugin() {
        return mPipePlugin;
    }

    @Override
    public String toString() {
        String s = getClass().getName();
        String message = getLocalizedMessage();
        return (s + ": " + mPipePlugin.getClass().getName() + ": " + message);
    }
}
