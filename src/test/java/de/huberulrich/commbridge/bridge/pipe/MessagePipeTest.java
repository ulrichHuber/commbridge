/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe;

import de.huberulrich.commbridge.bridge.pipe.plugin.PipePlugin;
import de.huberulrich.commbridge.exceptions.PipeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class MessagePipeTest {
    private MessagePipe messagePipe;
    private PipePlugin pipePlugin1;
    private PipePlugin pipePlugin2;
    private PipePlugin pipePlugin3;

    public MessagePipeTest() throws PipeException {
        pipePlugin1 = Mockito.mock(PipePlugin.class, Mockito.CALLS_REAL_METHODS);
        pipePlugin2 = Mockito.mock(PipePlugin.class, Mockito.CALLS_REAL_METHODS);
        pipePlugin3 = Mockito.mock(PipePlugin.class, Mockito.CALLS_REAL_METHODS);
    }

    @Test
    public void testAddPlugin() {
        messagePipe = new MessagePipe();
        messagePipe.addPlugin(pipePlugin1);
        messagePipe.addPlugin(pipePlugin2);
        messagePipe.addPlugin(1, pipePlugin3);
        assertArrayEquals(new PipePlugin[]{pipePlugin1, pipePlugin3, pipePlugin2}, messagePipe.getAllRegisteredPipePlugins());
    }

    @Test
    public void testRemovePlugin() {
        messagePipe = new MessagePipe();
        messagePipe.addPlugin(pipePlugin1);
        messagePipe.addPlugin(pipePlugin2);
        messagePipe.addPlugin(1, pipePlugin3);

        //remove plugin
        messagePipe.removePlugin(pipePlugin3);
        assertArrayEquals(new PipePlugin[]{pipePlugin1, pipePlugin2}, messagePipe.getAllRegisteredPipePlugins());

        messagePipe.removePlugin(pipePlugin1);
        assertArrayEquals(new PipePlugin[]{pipePlugin2}, messagePipe.getAllRegisteredPipePlugins());
    }

    @Test
    public void testApplyPipe() throws PipeException {
        messagePipe = new MessagePipe();
        messagePipe.addPlugin(pipePlugin1);
        messagePipe.addPlugin(pipePlugin2);
        messagePipe.addPlugin(pipePlugin3);

        messagePipe.applyPipe(new byte[]{});
        InOrder inOrder = Mockito.inOrder(pipePlugin1, pipePlugin2, pipePlugin3);
        inOrder.verify(pipePlugin1).apply(any());
        inOrder.verify(pipePlugin2).apply(any());
        inOrder.verify(pipePlugin3).apply(any());
    }

    @Test
    public void testUndoPipe() throws PipeException {
        messagePipe = new MessagePipe();
        messagePipe.addPlugin(pipePlugin1);
        messagePipe.addPlugin(pipePlugin2);
        messagePipe.addPlugin(pipePlugin3);

        messagePipe.undoPipe(new byte[]{});
        InOrder inOrder = Mockito.inOrder(pipePlugin1, pipePlugin2, pipePlugin3);
        inOrder.verify(pipePlugin3).undo(any());
        inOrder.verify(pipePlugin2).undo(any());
        inOrder.verify(pipePlugin1).undo(any());
    }
}
