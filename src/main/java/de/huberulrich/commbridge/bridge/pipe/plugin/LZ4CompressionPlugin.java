/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import de.huberulrich.commbridge.exceptions.PipeException;
import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Exception;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4SafeDecompressor;

import java.nio.ByteBuffer;

/**
 * PipePlugin that uses LZ4 to compress data.
 * LZ4 is very fast and has a reasonable compression ratio
 */
public class LZ4CompressionPlugin implements PipePlugin {
    private final LZ4Compressor compressor;
    private final LZ4SafeDecompressor decompressor;

    /**
     * Constructs a new {@link LZ4CompressionPlugin}
     */
    public LZ4CompressionPlugin() {
        LZ4Factory factory = LZ4Factory.fastestInstance();
        compressor = factory.fastCompressor();
        decompressor = factory.safeDecompressor();
    }

    /**
     * Compresses an array of bytes with LZ4 and prepends the length of the original array for decompression
     *
     * @param messageBytes the array of bytes
     * @return the compressed array of bytes
     * @throws PipeException Thrown if the LZ4 compressor promises a length of the compressed array that is not kept
     *                       while compressing. This would equal a buffer overflow.
     */
    @Override
    public byte[] apply(byte[] messageBytes) throws PipeException {
        int maxCompressedLength = compressor.maxCompressedLength(messageBytes.length);
        ByteBuffer byteBuffer = ByteBuffer.allocate(Integer.BYTES * 2 + maxCompressedLength);
        byteBuffer.putInt(messageBytes.length);
        try {
            byteBuffer.putInt(compressor.compress(messageBytes, 0, messageBytes.length, byteBuffer.array(),
                    Integer.BYTES * 2, maxCompressedLength));
        } catch (LZ4Exception e) {
            throw new PipeException(this, "The compressed data was actually longer than promised", e);
        }
        return byteBuffer.array();
    }

    /**
     * Decompresses an array of bytes using LZ4.
     *
     * @param compressedMessageBytes the compressed array with the decompressed and compressed length prepended
     * @return the decompressed array of bytes
     * @throws PipeException Thrown if the prepended length of the decompressed array is not kept while decompressing.
     *                       The decompression is aborted in this case prior to the buffer overflow.
     */
    @Override
    public byte[] undo(byte[] compressedMessageBytes) throws PipeException {
        ByteBuffer byteBuffer = ByteBuffer.wrap(compressedMessageBytes);
        int decompressedLength = byteBuffer.getInt();
        int compressedLength = byteBuffer.getInt();
        byte[] restoredMessageBytes = new byte[decompressedLength];
        try {
            decompressor.decompress(compressedMessageBytes, Integer.BYTES * 2, compressedLength, restoredMessageBytes, 0);
        } catch (ArrayIndexOutOfBoundsException | LZ4Exception e) {
            throw new PipeException(this, "Provided length of decompressed or compressed data was wrong.", e);
        }
        return restoredMessageBytes;
    }
}
