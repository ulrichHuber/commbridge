/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe;

import de.huberulrich.commbridge.bridge.pipe.plugin.PipePlugin;
import de.huberulrich.commbridge.exceptions.PipeException;

import java.util.Iterator;
import java.util.LinkedList;

public class MessagePipe {
    /**
     * The list of PipePlugins of this MessagePipe
     */
    private final LinkedList<PipePlugin> mPipePlugins = new LinkedList<>();

    /**
     * Adds a plugin to the end of the pipe
     *
     * @param pipePlugin the plugin
     */
    public void addPlugin(PipePlugin pipePlugin) {
        mPipePlugins.addLast(pipePlugin);
    }

    /**
     * Adds a plugin to the pipe at a specified index
     *
     * @param index      the index where the plugin should be added (zero-based)
     * @param pipePlugin the plugin
     */
    public void addPlugin(int index, PipePlugin pipePlugin) {
        mPipePlugins.add(index, pipePlugin);
    }

    /**
     * Removes the first occurrence of the PipePlugin in the MessagePipe
     *
     * @param pipePlugin the PipePlugin to remove
     */
    public void removePlugin(PipePlugin pipePlugin) {
        mPipePlugins.remove(pipePlugin);
    }

    public PipePlugin[] getAllRegisteredPipePlugins() {
        PipePlugin[] pipePlugins = new PipePlugin[mPipePlugins.size()];
        return mPipePlugins.toArray(pipePlugins);
    }

    /**
     * Runs the {@link PipePlugin#apply(byte[])} function of each {@link PipePlugin}
     *
     * @param messageBytes the bytes of the message to send through the pipe
     * @return the bytes of the message after running through the pipe
     */
    public byte[] applyPipe(byte[] messageBytes) throws PipeException {
        for (PipePlugin mPipePlugin : mPipePlugins)
            messageBytes = mPipePlugin.apply(messageBytes);
        return messageBytes;
    }

    /**
     * Runs the {@link PipePlugin#undo(byte[])} function of each {@link PipePlugin} in reverse order to {@link #applyPipe(byte[])}
     *
     * @param messageBytes the bytes of the message to send through the pipe
     * @return the bytes of the message after running through the pipe
     */
    public byte[] undoPipe(byte[] messageBytes) throws PipeException {
        Iterator<PipePlugin> pipeReverseIterator = mPipePlugins.descendingIterator();
        while (pipeReverseIterator.hasNext())
            messageBytes = pipeReverseIterator.next().undo(messageBytes);
        return messageBytes;
    }
}
