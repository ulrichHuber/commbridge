package de.huberulrich.commbridge.message;

import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class AcknowledgementTest {
    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForSenderId() {
        new Acknowledgement(null, new GlobalIdentifier("a:b:c"), UUID.randomUUID());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForRecipientId() {
        new Acknowledgement(new GlobalIdentifier("a:b:c"), null, UUID.randomUUID());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForOriginalMessageId() {
        new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), null);
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForSenderId() {
        try {
            new Acknowledgement(new GlobalIdentifier("*:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("a:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("*:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithExternalBroadcastForSenderId() {
        try {
            new Acknowledgement(new GlobalIdentifier("-:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("-:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForRecipientId() {
        try {
            new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:*"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:*"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithExternalBroadcastForRecipientId() {
        try {
            new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:b:c"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:*"), UUID.randomUUID());
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testGetOriginalMessageId() {
        UUID uuid = UUID.randomUUID();
        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), uuid);
        assertEquals(uuid, acknowledgement.getOriginalMessageId());
        assertNotEquals(uuid, acknowledgement.getMessageId());
    }

    @Test
    public void testGetMessageLength() {
        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
        assertEquals(acknowledgement.getMessageLength(), acknowledgement.getBytes().length);
    }

    @Test(expected = CloneNotSupportedException.class)
    public void testCloneWithNewBusIdentifierForNonBroadcast() throws CloneNotSupportedException {
        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
        acknowledgement.cloneWithNewBusIdentifier(new BusIdentifier("b"));
    }

    @Test
    public void testTransformByteArrayReversible() throws ParseMessageException {
        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID());
        byte[] bytes = acknowledgement.getBytes();
        Acknowledgement restoredAcknowledgement = Acknowledgement.restoreFromBytes(bytes);

        assertEquals(acknowledgement.getSenderIdentifier(), restoredAcknowledgement.getSenderIdentifier());
        assertEquals(acknowledgement.getRecipientIdentifier(), restoredAcknowledgement.getRecipientIdentifier());
        assertEquals(acknowledgement.getMessageId(), restoredAcknowledgement.getMessageId());
        assertEquals(acknowledgement.getOriginalMessageId(), restoredAcknowledgement.getOriginalMessageId());
    }
}
