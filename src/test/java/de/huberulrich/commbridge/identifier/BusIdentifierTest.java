/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.identifier;

import org.junit.Test;

import static org.junit.Assert.*;

public class BusIdentifierTest {
    @Test
    public void testValidIdentifier() {
        new BusIdentifier("123");
        new BusIdentifier("abc123");
        new BusIdentifier("ABCabc123");
        new BusIdentifier("*");
        new BusIdentifier("-");
    }

    @Test
    public void testMaximumIdentifierLength() {
        new BusIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyIdentifier() {
        new BusIdentifier("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier() {
        new BusIdentifier(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDisallowedCharacterIdentifier() {
        new BusIdentifier(".");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooLongIdentifier() {
        new BusIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLineBreakIdentifier() {
        new BusIdentifier("\n");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidWithWhitespaceIdentifier() {
        new BusIdentifier("abc ");
    }

    @Test
    public void testIsGlobalBroadcast() {
        assertTrue(new BusIdentifier("*").isGlobalBroadcast());
        assertFalse(new BusIdentifier("-").isGlobalBroadcast());
        assertFalse(new BusIdentifier("abc").isGlobalBroadcast());
    }

    @Test
    public void testIsExternalBroadcast() {
        assertTrue(new BusIdentifier("-").isExternalBroadcast());
        assertFalse(new BusIdentifier("*").isExternalBroadcast());
        assertFalse(new BusIdentifier("abc").isExternalBroadcast());
    }

    @Test
    public void testToString() {
        assertEquals("abc", new BusIdentifier("abc").toString());
    }

    @Test
    public void testEquals() {
        assertEquals(new BusIdentifier("abc"), new BusIdentifier("abc"));
        assertNotEquals(new BusIdentifier("abc"), new BusIdentifier("def"));
    }

    @Test
    public void testRepeatableHashCode() {
        assertEquals(new BusIdentifier("abc").hashCode(), new BusIdentifier("abc").hashCode());
        assertNotEquals(new BusIdentifier("abc").hashCode(), new BusIdentifier("def").hashCode());
    }
}
