package de.huberulrich.commbridge.message;

import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.plugin.Task;
import org.junit.Test;

import static org.junit.Assert.*;

public class RequestTest {
    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForSenderId() {
        new Request(null, new GlobalIdentifier("a:b:c"), new Task("task"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForRecipientId() {
        new Request(new GlobalIdentifier("a:b:c"), null, new Task("task"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForTask() {
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), null, null);
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForSenderId() {
        try {
            new Request(new GlobalIdentifier("*:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Request(new GlobalIdentifier("a:*"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Request(new GlobalIdentifier("*:*"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithExternalBroadcastForSenderId() {
        try {
            new Request(new GlobalIdentifier("-:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Request(new GlobalIdentifier("-:*"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForRecipientId() {
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:b:c"), new Task("task"), null);
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:*"), new Task("task"), null);
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:*"), new Task("task"), null);
    }

    @Test
    public void testInstantiationWithExternalBroadcastForRecipientId() {
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:b:c"), new Task("task"), null);
        new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:*"), new Task("task"), null);
    }

    @Test
    public void testGetTask() {
        Task task = new Task("task");
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), task, null);
        assertEquals(task, request.getTask());
    }

    @Test
    public void testGetData() {
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
        assertNull(request.getData());
        request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), new byte[]{1, 2, 3});
        assertArrayEquals(new byte[]{1, 2, 3}, request.getData());
    }

    @Test
    public void testGetMessageLengthWithoutData() {
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
        assertEquals(request.getMessageLength(), request.getBytes().length);
    }

    @Test
    public void testGetMessageLengthWithData() {
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), new byte[]{1, 2, 3});
        assertEquals(request.getMessageLength(), request.getBytes().length);
    }

    @Test
    public void testCloneWithNewBusIdentifierForBroadcast() throws CloneNotSupportedException {
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:b:c"), new Task("task"), null);
        request.cloneWithNewBusIdentifier(new BusIdentifier("b"));
        request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:b:c"), new Task("task"), null);
        request.cloneWithNewBusIdentifier(new BusIdentifier("b"));
    }

    @Test(expected = CloneNotSupportedException.class)
    public void testCloneWithNewBusIdentifierForNonBroadcast() throws CloneNotSupportedException {
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), null);
        request.cloneWithNewBusIdentifier(new BusIdentifier("b"));
    }

    @Test
    public void testTransformByteArrayReversible() throws ParseMessageException {
        byte[] data = new byte[]{1, 2, 3, 4, 5};
        Request request = new Request(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), new Task("task"), data);
        byte[] bytes = request.getBytes();
        Request restoredRequest = Request.restoreFromBytes(bytes);

        assertEquals(request.getSenderIdentifier(), restoredRequest.getSenderIdentifier());
        assertEquals(request.getRecipientIdentifier(), restoredRequest.getRecipientIdentifier());
        assertEquals(request.getMessageId(), restoredRequest.getMessageId());
        assertEquals(request.getTask(), restoredRequest.getTask());
        assertArrayEquals(request.getData(), restoredRequest.getData());
    }
}
