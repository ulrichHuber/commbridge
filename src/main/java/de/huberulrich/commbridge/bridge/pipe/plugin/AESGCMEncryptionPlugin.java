/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import de.huberulrich.commbridge.exceptions.PipeException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * A PipePlugin that encrypts/decrypts byte streams with AES in GCM mode. Authentication (AEAD) can be enabled.
 */
public class AESGCMEncryptionPlugin implements PipePlugin {

    private static final int AES_KEY_SIZE = 128; // in bits
    private static final int GCM_NONCE_LENGTH = 12; // in bytes
    private static final int GCM_TAG_LENGTH = 16; // in bytes


    private final Cipher mCipher;
    private final SecretKey mSymKey;
    private final SecureRandom rnd;

    /**
     * Constructs a PipePlugin that encrypts/decrypts byte streams with AES in GCM mode. Authentication is not provided.
     *
     * @param symKey The symmetric key used for encryption/decryption of the byte stream
     * @throws PipeException Thrown when encryption/decryption fails
     */
    public AESGCMEncryptionPlugin(SecretKey symKey) throws PipeException {
        try {
            mCipher = Cipher.getInstance("AES/GCM/NoPadding");
            rnd = SecureRandom.getInstanceStrong();
            mSymKey = symKey;
        } catch (NoSuchAlgorithmException e) {
            throw new PipeException(this, "No Provider for the AES algorithm found");
        } catch (NoSuchPaddingException e) {
            throw new PipeException(this, "No Provider for NoPadding found");
        }
    }

    /**
     * Generates a random SecretKey that can be used with this Plugin
     *
     * @return the random secret key
     * @throws NoSuchAlgorithmException Thrown when no Provider for AES is registered
     */
    public static SecretKey generateRandomSecretKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(AES_KEY_SIZE, SecureRandom.getInstanceStrong());
        return keyGen.generateKey();
    }

    /**
     * Encrypts an array of bytes using AES in GCM mode
     *
     * @param messageBytes the array of bytes
     * @return the encrypted array of bytes
     * @throws PipeException Thrown when an error occurs while encrypting the array
     */
    @Override
    public byte[] apply(byte[] messageBytes) throws PipeException {

        try {
            // generate random IV using block size
            final byte[] nonce = new byte[GCM_NONCE_LENGTH];
            rnd.nextBytes(nonce);
            final GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, nonce);
            mCipher.init(Cipher.ENCRYPT_MODE, mSymKey, spec);

            mCipher.updateAAD(nonce);

            final byte[] encryptedMessage = mCipher.doFinal(messageBytes);

            // concatenate IV and encrypted message
            final byte[] ivAndEncryptedMessage = new byte[nonce.length + encryptedMessage.length];
            System.arraycopy(nonce, 0, ivAndEncryptedMessage, 0, GCM_NONCE_LENGTH);
            System.arraycopy(encryptedMessage, 0, ivAndEncryptedMessage, GCM_NONCE_LENGTH, encryptedMessage.length);

            return ivAndEncryptedMessage;
        } catch (InvalidKeyException e) {
            throw new PipeException(this, "AES key invalid", e);
        } catch (GeneralSecurityException e) {
            throw new PipeException(this, "Unexpected exception during encryption", e);
        }
    }

    /**
     * Decrypts an array of bytes using AES in GCM mode
     *
     * @param messageBytes the encrypted array of bytes
     * @return the decrypted array of bytes
     * @throws PipeException Thrown when an error occurs while decrypting the array
     */
    @Override
    public byte[] undo(byte[] messageBytes) throws PipeException {

        try {
            // retrieve random IV from start of the received message
            final byte[] nonce = new byte[GCM_NONCE_LENGTH];
            System.arraycopy(messageBytes, 0, nonce, 0, GCM_NONCE_LENGTH);
            final GCMParameterSpec spec = new GCMParameterSpec(GCM_TAG_LENGTH * Byte.SIZE, nonce);

            // retrieve the encrypted message itself
            final byte[] encryptedMessage = new byte[messageBytes.length - GCM_NONCE_LENGTH];
            System.arraycopy(messageBytes, GCM_NONCE_LENGTH, encryptedMessage, 0, encryptedMessage.length);

            mCipher.init(Cipher.DECRYPT_MODE, mSymKey, spec);

            mCipher.updateAAD(nonce);

            return mCipher.doFinal(encryptedMessage);
        } catch (InvalidKeyException e) {
            throw new PipeException(this, "AES key invalid", e);
        } catch (GeneralSecurityException e) {
            throw new PipeException(this, "Unexpected exception during decryption", e);
        }
    }
}
