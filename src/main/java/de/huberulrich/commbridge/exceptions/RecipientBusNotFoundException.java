/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.exceptions;

import de.huberulrich.commbridge.identifier.BusIdentifier;

/**
 * This exception is thrown when no recipient bus was found for a message.
 */
public class RecipientBusNotFoundException extends Exception {
    private final BusIdentifier mBusIdentifier;

    /**
     * Constructs a new Exception
     *
     * @param busIdentifier The recipient that was not found
     */
    public RecipientBusNotFoundException(BusIdentifier busIdentifier) {
        super();
        mBusIdentifier = busIdentifier;
    }

    /**
     * Gets the BusIdentifier of the bus that was not found
     *
     * @return the bus identifier
     */
    public BusIdentifier getBusIdentifier() {
        return mBusIdentifier;
    }
}
