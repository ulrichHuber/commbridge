/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import com.fasterxml.uuid.Generators;
import de.huberulrich.commbridge.exceptions.PipeException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import static org.junit.Assert.*;

@RunWith(MockitoJUnitRunner.class)
public class AESGCMEncryptionPluginTest {
    private AESGCMEncryptionPlugin aesgcmEncryptionPlugin;
    private byte[] messageBytes;

    public AESGCMEncryptionPluginTest() throws PipeException {
        Random random = new Random(Generators.timeBasedGenerator().generate().getMostSignificantBits());
        SecretKeySpec secretKeySpec = new SecretKeySpec(new byte[]{62, -41, 124, -104, 1, -29, 78, -58, -4, -52, 112, -46, 124, -54, -67, -128}, "AES");
        aesgcmEncryptionPlugin = new AESGCMEncryptionPlugin(secretKeySpec);
        messageBytes = new byte[400];
        random.nextBytes(messageBytes);
    }

    @Test
    public void testGenerateRandomSecretKey() throws NoSuchAlgorithmException {
        assertNotNull(AESGCMEncryptionPlugin.generateRandomSecretKey());
    }

    @Test
    public void testApplyAndUndoAreMatchingOperations() {
        try {
            assertArrayEquals(messageBytes, aesgcmEncryptionPlugin.undo(aesgcmEncryptionPlugin.apply(messageBytes)));
        } catch (PipeException e) {
            fail();
        }
    }

    @Test(expected = PipeException.class)
    public void testMessageCorruption() throws PipeException {
        byte[] encBytes = aesgcmEncryptionPlugin.apply(messageBytes);
        encBytes[2] = (byte) ((encBytes[2] + 5) % Byte.MAX_VALUE);
        aesgcmEncryptionPlugin.undo(encBytes);
    }

    @Test
    public void testWrongKey() throws PipeException {
        SecretKeySpec secretKeySpec = new SecretKeySpec(new byte[]{62, -41, 124, -104, 1, -29, 78, -58, -4, -52, 112, -46, 124, -54, -67, -2}, "SHA");
        AESGCMEncryptionPlugin plugin = new AESGCMEncryptionPlugin(secretKeySpec);

        try {
            plugin.apply(messageBytes);
            fail();
        } catch (Exception ignored) {
        }

        byte[] encBytes = aesgcmEncryptionPlugin.apply(messageBytes);
        encBytes[2] = (byte) ((encBytes[2] + 5) % Byte.MAX_VALUE);

        try {
            plugin.undo(encBytes);
            fail();
        } catch (Exception ignored) {
        }
    }
}
