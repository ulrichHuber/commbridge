/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.identifier;

import org.junit.Test;

import java.nio.charset.StandardCharsets;

import static org.junit.Assert.*;

public class GlobalIdentifierTest {
    @Test
    public void testConstructorsEqual() {
        assertEquals(new GlobalIdentifier("abc:def:ghi"),
                new GlobalIdentifier(new BusIdentifier("abc"), new PluginIdentifier("def:ghi")));
    }

    @Test
    public void testValidIdentifier() {
        new GlobalIdentifier("123:456:789");
        new GlobalIdentifier("abc123:def456:ghi789");
        new GlobalIdentifier("ABCabc123:DEFdef456:GHIghi789");
        new GlobalIdentifier("*:123:456");
        new GlobalIdentifier("-:123:456");
        new GlobalIdentifier("*:*");
        new GlobalIdentifier("-:*");
    }

    @Test
    public void testMaximumIdentifierLength() {
        new GlobalIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ghijklmnopqrstuvwxyz1234567890AB");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testEmptyIdentifier() {
        new GlobalIdentifier("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier() {
        new GlobalIdentifier(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier2() {
        new GlobalIdentifier(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier3() {
        new GlobalIdentifier(new BusIdentifier("a"), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNullIdentifier4() {
        new GlobalIdentifier(null, new PluginIdentifier("a:b"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDisallowedCharacterIdentifier() {
        new GlobalIdentifier(".:def:ghi");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooLongBusIdentifier() {
        new GlobalIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefg:ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ghijklmnopqrstuvwxyz1234567890AB");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testTooLongPluginIdentifier() {
        new GlobalIdentifier("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef:ghijklmnopqrstuvwxyz1234567890ABC");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLineBreakIdentifier() {
        new GlobalIdentifier("\n");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidWithWhitespaceIdentifier() {
        new GlobalIdentifier("abc :def:ghi");
    }

    @Test
    public void testGetBusIdentifier() {
        assertEquals(new BusIdentifier("abc"), new GlobalIdentifier("abc:def:ghi").getBusIdentifier());
    }

    @Test
    public void testGetPluginIdentifier() {
        assertEquals(new PluginIdentifier("def:ghi"), new GlobalIdentifier("abc:def:ghi").getPluginIdentifier());
    }

    @Test
    public void testGetBytes() {
        assertArrayEquals("abc:def:ghi".getBytes(StandardCharsets.UTF_8), new GlobalIdentifier("abc:def:ghi").getBytes());
    }

    @Test
    public void testRestoreFromBytes() {
        assertEquals(new GlobalIdentifier("abc:def:ghi"), GlobalIdentifier.restoreFromBytes("abc:def:ghi".getBytes(StandardCharsets.UTF_8)));
    }

    @Test
    public void testToString() {
        assertEquals("abc:def:ghi", new GlobalIdentifier("abc:def:ghi").toString());
    }

    @Test
    public void testEquals() {
        assertEquals(new GlobalIdentifier("abc:def:ghi"), new GlobalIdentifier("abc:def:ghi"));
        assertNotEquals(new GlobalIdentifier("abc:def:ghi"), new GlobalIdentifier("jkl:mno:pqr"));
    }

    @Test
    public void testRepeatableHashCode() {
        assertEquals(new GlobalIdentifier("abc:def:ghi").hashCode(), new GlobalIdentifier("abc:def:ghi").hashCode());
        assertNotEquals(new GlobalIdentifier("abc:def:ghi").hashCode(), new GlobalIdentifier("jkl:mno:pqr").hashCode());
    }
}
