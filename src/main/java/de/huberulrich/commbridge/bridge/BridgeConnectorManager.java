/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.exceptions.RecipientBusNotFoundException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.message.Message;
import org.apache.commons.collections4.multimap.HashSetValuedHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ExceptionListener;
import java.util.ArrayList;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * The manager for all BridgeConnectors. This manager handles the routes made available by the connectors and forwards
 * messages to the appropriate bridges.
 */
public class BridgeConnectorManager {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(BridgeConnectorManager.class);

    /**
     * Internal synchronization object
     */
    private ReadWriteLock lock = new ReentrantReadWriteLock(true);

    /**
     * Map with all registered BridgeConnectors identified by available buses
     */
    private final HashSetValuedHashMap<BusIdentifier, BridgeConnector> mBridgeConnectorsByBus = new HashSetValuedHashMap<>();

    /**
     * Set of all registered BridgeConnectors
     */
    private final Set<BridgeConnector> mBridgeConnectors = ConcurrentHashMap.newKeySet();

    /**
     * The bus all connectors shall be associated with
     */
    private final Bus mBus;

    /**
     * Constructs a new BridgeConnectorManager associated to the provided Bus
     *
     * @param bus the Bus
     */
    public BridgeConnectorManager(Bus bus) {
        if (bus == null)
            throw new IllegalArgumentException("Bus cannot be null");

        mBus = bus;

        log.info("[Bus:" + mBus.getBusIdentifier() + "] New BridgeConnectorManager created");
    }

    /**
     * Registers a BridgeConnector to this manager. Messages will from this point on be received and pushed to the bus,
     * as well as messages on the bus sent via this connector when appropriate.
     *
     * @param bridgeConnector the connector
     */
    public void registerBridgeConnector(BridgeConnector bridgeConnector) {
        log.info("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Registering new BridgeConnector " + bridgeConnector.getReadableName());

        bridgeConnector.setBridgeConnectorManager(this);
        mBridgeConnectors.add(bridgeConnector);
        bridgeConnectionStatusChanged(bridgeConnector);

        log.debug("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] BridgeConnector registered");
    }

    /**
     * Unregisters a BridgeConnector from this manager. Messages will neither be sent nor received via this connector.
     *
     * @param bridgeConnector the connector to unregister
     */
    public void unregisterBridgeConnector(BridgeConnector bridgeConnector) {
        lock.writeLock().lock();
        try {
            log.info("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Unregistering BridgeConnector " + bridgeConnector.getReadableName());

            bridgeConnector.setBridgeConnectorManager(null);
            mBridgeConnectors.remove(bridgeConnector);
            for(BusIdentifier busIdentifier : bridgeConnector.getConnectedBusIdentifiers())
                mBridgeConnectorsByBus.removeMapping(busIdentifier, bridgeConnector);

            log.debug("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] BridgeConnector unregistered");
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Returns the identifier of the Bus this BridgeConnectorManager is connected to.
     *
     * @return the identifier
     */
    BusIdentifier getConnectedBusIdentifier() {
        return mBus.getBusIdentifier();
    }

    /**
     * Returns all registered connectors
     *
     * @return an array of connectors
     */
    public BridgeConnector[] getAllRegisteredBridges() {
        BridgeConnector[] bridgeConnectors = new BridgeConnector[mBridgeConnectors.size()];
        return mBridgeConnectors.toArray(bridgeConnectors);
    }

    /**
     * Returns all known external Buses
     *
     * @return an array of all known external Buses
     */
    public BusIdentifier[] getAllKnownExternalBusIdentifiers() {
        BusIdentifier[] busIdentifiers = new BusIdentifier[mBridgeConnectorsByBus.size()];
        return mBridgeConnectorsByBus.keySet().toArray(busIdentifiers);
    }

    /**
     * Called by BridgeConnectors when their connection status changed (the list of connected buss changed)
     *
     * @param bridgeConnector the connector that changed
     */
    void bridgeConnectionStatusChanged(BridgeConnector bridgeConnector) {
        lock.writeLock().lock();
        try {
            log.debug("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Updating list of known routes to buses");
            log.trace("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Update notified by " + bridgeConnector.getReadableName());
            ArrayList<BusIdentifier> identifiers = bridgeConnector.getConnectedBusIdentifiers();

            log.trace("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Old list of known routes to buses: " + mBridgeConnectorsByBus.toString());

            //remove all old buses this connector was connected to
            mBridgeConnectorsByBus.entries().removeIf(busIdentifierBridgeConnectorEntry -> busIdentifierBridgeConnectorEntry.getValue().equals(bridgeConnector));

            //add all the new buses connected by this connector
            for (BusIdentifier identifier : identifiers)
                mBridgeConnectorsByBus.put(identifier, bridgeConnector);

            log.trace("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] New list of known routes to buses: " + mBridgeConnectorsByBus.toString());
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Parses a message from an array of bytes and pushes it to the Bus.
     *
     * @param messageBytes the message in bytes
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          ParseMessageException: Thrown when the Message could not be parsed
     *                          </li>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    void sendMessageToBus(byte[] messageBytes, ExceptionListener exceptionListener) {
        mBus.sendMessage(messageBytes, exceptionListener);
    }


    /**
     * Resolves the best bridges for the recipients, and sends the message.
     *
     * @param message the message
     */
    public void outgoingMessage(Message message) throws RecipientBusNotFoundException, PipeException {
        lock.readLock().lock();
        try {
            String logAppend = "[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager][ID:" + message.getMessageId() + "] ";
            log.info(logAppend + "Delivering outgoing Message");

            BusIdentifier recipientBusIdentifier = message.getRecipientIdentifier().getBusIdentifier();
            if (recipientBusIdentifier.isGlobalBroadcast() || recipientBusIdentifier.isExternalBroadcast()) {
                //We want the best bridge for every bus

                //If the message is a broadcast only to external buses we throw an exception if no external bus is connected
                if (recipientBusIdentifier.isExternalBroadcast() && mBridgeConnectorsByBus.keySet().isEmpty()) {
                    log.warn(logAppend + "Outgoing Message was an external broadcast but no other buses are connected");
                    throw new RecipientBusNotFoundException(message.getRecipientIdentifier().getBusIdentifier());
                }

                for (BusIdentifier busIdentifier : mBridgeConnectorsByBus.keySet()) {
                    log.debug(logAppend + "Deliver outgoing Message to bus " + recipientBusIdentifier.toString());
                    //Change the recipient of the message to the bus. Otherwise the message would just be relayed by the bus again.
                    try {
                        BridgeConnector bridgeConnector = getBestBridgeConnectorForBus(busIdentifier, message.getMessageLength());
                        bridgeConnector.pushThroughPipeToOutgoing(message.cloneWithNewBusIdentifier(busIdentifier));
                    } catch (CloneNotSupportedException ignored) {
                        //This will never happen, since we have already checked that the message is a broadcast and therefore cloneable
                    }
                }
            } else {
                //We want one specific bus
                log.debug(logAppend + "Deliver outgoing Message to bus " + recipientBusIdentifier.toString());

                getBestBridgeConnectorForBus(recipientBusIdentifier, message.getMessageLength()).pushThroughPipeToOutgoing(message);
            }
            log.debug(logAppend + "Delivery of outgoing Message finished");
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * Selects the bridge with the best priority for the given bus
     *
     * @param busIdentifier the bus
     * @return the best bridge
     */
    private BridgeConnector getBestBridgeConnectorForBus(BusIdentifier busIdentifier, int dataLength) throws RecipientBusNotFoundException {
        lock.readLock().lock();
        try {
            log.debug("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager]Searching for best route to bus with identifier " + busIdentifier.toString());

            SortedSet<BridgeConnector> sortedBridgeConnectors = new TreeSet<>(new BridgeConnectorPriorityComparator(dataLength));
            sortedBridgeConnectors.addAll(mBridgeConnectorsByBus.get(busIdentifier));
            log.trace("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager]Resulting priority sorted list of direct routes: " + sortedBridgeConnectors.toString());

            //check if we found a direct or indirect route to the bus
            if (sortedBridgeConnectors.isEmpty()) {
                log.error("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager]No route to directly addressed recipient of message was found");
                throw new RecipientBusNotFoundException(busIdentifier);
            }

            log.debug("[Bus:" + mBus.getBusIdentifier() + "][BridgeConnectorManager] Selected BridgeConnector: " + sortedBridgeConnectors.first().getReadableName());

            //return the first and therefore best bridge to this bus
            return sortedBridgeConnectors.first();
        } finally {
            lock.readLock().unlock();
        }
    }
}
