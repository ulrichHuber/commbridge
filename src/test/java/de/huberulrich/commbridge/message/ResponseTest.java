package de.huberulrich.commbridge.message;

import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import org.junit.Test;

import java.util.UUID;

import static org.junit.Assert.*;

public class ResponseTest {
    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForSenderId() {
        new Response(null, new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForRecipientId() {
        new Response(new GlobalIdentifier("a:b:c"), null, UUID.randomUUID(), null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testInstantiationWithNullForOriginalMessageId() {
        new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), null, null);
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForSenderId() {
        try {
            new Response(new GlobalIdentifier("*:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("a:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("*:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithExternalBroadcastForSenderId() {
        try {
            new Response(new GlobalIdentifier("-:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("-:*"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithGlobalBroadcastForRecipientId() {
        try {
            new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:*"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("*:*"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testInstantiationWithExternalBroadcastForRecipientId() {
        try {
            new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:b:c"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }

        try {
            new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("-:*"), UUID.randomUUID(), null);
            fail();
        } catch (IllegalArgumentException ignored) {
        }
    }

    @Test
    public void testGetOriginalMessageId() {
        UUID uuid = UUID.randomUUID();
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), uuid, null);
        assertEquals(uuid, response.getOriginalMessageId());
    }

    @Test
    public void testGetData() {
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
        assertNull(response.getData());
        response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), new byte[]{1, 2, 3});
        assertArrayEquals(new byte[]{1, 2, 3}, response.getData());
    }

    @Test
    public void testGetMessageLengthWithoutData() {
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
        assertEquals(response.getMessageLength(), response.getBytes().length);
    }

    @Test
    public void testGetMessageLengthWithData() {
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), new byte[]{1, 2, 3});
        assertEquals(response.getMessageLength(), response.getBytes().length);
    }

    @Test(expected = CloneNotSupportedException.class)
    public void testCloneWithNewBusIdentifierForNonBroadcast() throws CloneNotSupportedException {
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), null);
        response.cloneWithNewBusIdentifier(new BusIdentifier("b"));
    }

    @Test
    public void testTransformByteArrayReversible() throws ParseMessageException {
        byte[] data = new byte[]{1, 2, 3, 4, 5};
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("a:b:c"), UUID.randomUUID(), data);
        byte[] bytes = response.getBytes();
        Response restoredResponse = Response.restoreFromBytes(bytes);

        assertEquals(response.getSenderIdentifier(), restoredResponse.getSenderIdentifier());
        assertEquals(response.getRecipientIdentifier(), restoredResponse.getRecipientIdentifier());
        assertEquals(response.getMessageId(), restoredResponse.getMessageId());
        assertEquals(response.getOriginalMessageId(), restoredResponse.getOriginalMessageId());
        assertArrayEquals(response.getData(), restoredResponse.getData());
    }
}

