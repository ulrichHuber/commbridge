/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.message;

/**
 * Interface for a listener for responses
 */
public interface ResponseListener {
    /**
     * This method will be called when an Acknowledgement was received as an response to the message.
     *
     * @param acknowledgement the acknowledgement
     */
    void onAcknowledgementReceived(Acknowledgement acknowledgement);

    /**
     * This method will be called when a Response was received as an response to the message.
     *
     * @param response the response
     */
    void onResponseReceived(Response response);

    /**
     * This method will be called, when the listener has existed for a specified time
     * and there has never been received any Message as response.
     * <p>
     * When this method is called, this means one of three things:
     * <ul>
     * <li>When the message was addressed to a specific recipient, that this recipient is not attached to the bus</li>
     * <li>When the message was a broadcast, that no plugin is attached to the bus that can handle the task specified in
     * the message</li>
     * <li>While transmitting the message via a bridge to another bus, an unrecoverable error occurred and the message
     * was lost</li>
     * </ul>
     */
    void onTimeout();
}
