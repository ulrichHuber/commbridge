/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bus;

import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.message.Message;
import de.huberulrich.commbridge.message.Request;
import de.huberulrich.commbridge.message.ResponseMessage;
import de.huberulrich.commbridge.plugin.Plugin;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ExceptionListener;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * {@link Bus} that handles Messages asynchronously
 */
public class AsyncBus extends Bus {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(AsyncBus.class);

    /**
     * The ThreadPoolExecutor used for sending Messages asynchronously
     */
    private ExecutorService mMessageExecutor;

    /**
     * The ThreadPoolExecutor used for processing the Messages within Plugins asynchronously
     */
    private ExecutorService mPluginExecutor;

    /**
     * Constructor for a new Bus that handles Messages asynchronously
     *
     * @param busIdentifier the identifier of this bus. The identifier needs to be globally unique or messages will not
     *                      be reliably delivered to this bus.
     */
    public AsyncBus(BusIdentifier busIdentifier) {
        super(busIdentifier);
        mMessageExecutor = Executors.newFixedThreadPool(2);
        mPluginExecutor = Executors.newCachedThreadPool();
    }

    /**
     * Constructor for a new Bus that handles Messages asynchronously
     *
     * @param busIdentifier          the identifier of this bus. The identifier needs to be globally unique or messages will not
     *                               be reliably delivered to this bus.
     * @param messageExecutorService the ExecutorService used for handling new Messages asynchronously
     * @param pluginExecutorService  the ExecutorService used for processing Messages in Plugins asynchronously
     */
    public AsyncBus(BusIdentifier busIdentifier, ExecutorService messageExecutorService, ExecutorService pluginExecutorService) {
        super(busIdentifier);
        if (messageExecutorService == null || pluginExecutorService == null)
            throw new IllegalArgumentException("Executors must not be null");
        mMessageExecutor = messageExecutorService;
        mPluginExecutor = pluginExecutorService;
    }

    /**
     * Stops the ExecutorServices used by the AsyncBus. The Bus will be unable to send new Messages or process Messages
     * that have not yet been processed, instantly after calling this method. The method blocks until the Bus is
     * stopped.
     *
     * @param timeout The timeout in seconds until which the Bus can send and process Messages that are already in the
     *                queue. Be aware that this method can take twice the timeout in worst case.
     */
    public void stopBus(int timeout) {
        log.error("[Bus:" + mBusIdentifier + "] Shutting down Bus");
        try {
            mMessageExecutor.shutdown();
            mPluginExecutor.shutdown();
            mMessageExecutor.awaitTermination(timeout, TimeUnit.SECONDS);
            mPluginExecutor.awaitTermination(timeout, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            mMessageExecutor.shutdownNow();
            mPluginExecutor.shutdownNow();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        stopBus(0);
        super.finalize();
    }

    @Override
    public void sendMessage(byte[] messageBytes, ExceptionListener exceptionListener) {
        try {
            mMessageExecutor.submit(() -> {
                super.sendMessage(messageBytes, exceptionListener);
            });
        } catch (RejectedExecutionException e) {
            log.error("[Bus:" + mBusIdentifier + "] Rejected new incoming Message since AsyncBus is shut down");
        }
    }

    @Override
    public void sendMessage(Message message, final ExceptionListener exceptionListener) {
        try {
            mMessageExecutor.submit(() -> {
                super.sendMessage(message, exceptionListener);
            });
        } catch (RejectedExecutionException e) {
            log.error("[Bus:" + mBusIdentifier + "] Rejected new incoming Message since AsyncBus is shut down");
        }
    }

    @Override
    protected void sendRequestToPlugin(Plugin plugin, Request request) {
        try {
            mPluginExecutor.submit(() -> {
                super.sendRequestToPlugin(plugin, request);
            });
        } catch (RejectedExecutionException e) {
            log.error("[Bus:" + mBusIdentifier + "] Rejected processing of Request since AsyncBus is shut down");
        }
    }

    @Override
    protected void sendResponseToPlugin(Plugin plugin, ResponseMessage responseMessage) {
        try {
            mPluginExecutor.submit(() -> {
                super.sendResponseToPlugin(plugin, responseMessage);
            });
        } catch (RejectedExecutionException e) {
            log.error("[Bus:" + mBusIdentifier + "] Rejected processing of ResponseMessage since AsyncBus is shut down");
        }
    }
}
