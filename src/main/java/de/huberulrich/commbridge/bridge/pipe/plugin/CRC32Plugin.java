/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge.pipe.plugin;

import de.huberulrich.commbridge.exceptions.PipeException;

import java.nio.ByteBuffer;
import java.util.zip.CRC32;

/**
 * A PipePlugin that generates and validates CRC32
 */
public class CRC32Plugin implements PipePlugin {

    /**
     * Prepends a CRC32 to the provided array of bytes
     *
     * @param messageBytes the array of bytes
     * @return array of bytes with CRC32 prepended
     */
    @Override
    public byte[] apply(byte[] messageBytes) {
        CRC32 crc = new CRC32();
        crc.update(messageBytes, 0, messageBytes.length);

        ByteBuffer messageBuffer = ByteBuffer.allocate(Long.BYTES + messageBytes.length);
        messageBuffer.putLong(crc.getValue());
        messageBuffer.put(messageBytes);

        return messageBuffer.array();
    }

    /**
     * Removes the prepended CRC32 from the array of bytes and checks it against the CRC32 of the message
     *
     * @param messageBytes the array of bytes
     * @return the array of bytes without the CRC32
     * @throws PipeException Thrown if the CRC32 do not match e.g. when a bit corruption occurred
     */
    @Override
    public byte[] undo(byte[] messageBytes) throws PipeException {
        ByteBuffer messageBuffer = ByteBuffer.wrap(messageBytes);
        long messageCRC = messageBuffer.getLong();

        byte[] actualMessage = new byte[messageBuffer.remaining()];
        messageBuffer.get(actualMessage, 0, messageBuffer.remaining());

        CRC32 crc = new CRC32();
        crc.update(actualMessage, 0, actualMessage.length);
        if (crc.getValue() != messageCRC)
            throw new PipeException(this, "Found Bit-Corruption while checking CRC32");

        return actualMessage;
    }
}
