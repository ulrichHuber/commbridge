/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bus;

import de.huberulrich.commbridge.bridge.BridgeConnectorManager;
import de.huberulrich.commbridge.exceptions.ParseMessageException;
import de.huberulrich.commbridge.exceptions.PipeException;
import de.huberulrich.commbridge.exceptions.RecipientBusNotFoundException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.message.*;
import de.huberulrich.commbridge.plugin.Plugin;
import de.huberulrich.commbridge.plugin.PluginManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ExceptionListener;
import java.util.ArrayList;

/**
 * The bus connects all registered plugins and bridges. It forwards messages pushed to the bus to all intended
 * recipients. While doing this, it keeps the message confidential between sender and recipient.
 */
public class Bus {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(Bus.class);

    /**
     * The BusIdentifier of this Bus
     */
    final BusIdentifier mBusIdentifier;

    /**
     * The BridgeConnectorManager connected to this Bus
     */
    private BridgeConnectorManager mBridgeConnectorManager;

    /**
     * The PluginManager connected to this Bus
     */
    private PluginManager mPluginManager;

    /**
     * Constructor for a new Bus
     *
     * @param busIdentifier the identifier of this bus. The identifier needs to be globally unique or messages will not
     *                      be reliably delivered to this bus.
     */
    public Bus(BusIdentifier busIdentifier) {
        if (busIdentifier == null)
            throw new IllegalArgumentException("The BusIdentifier cannot be null");
        log.info("New Bus with Identifier: " + busIdentifier);

        if (busIdentifier.isExternalBroadcast() || busIdentifier.isGlobalBroadcast()) {
            log.fatal("The provided identifier for the bus contained whitespace characters which is prohibited");
            throw new IllegalArgumentException("The provided identifier for the bus contained whitespace characters which is prohibited");
        }

        mBusIdentifier = busIdentifier;
        mPluginManager = new PluginManager(this);
        mBridgeConnectorManager = new BridgeConnectorManager(this);
    }

    /**
     * Get the identifier of this bus
     *
     * @return the identifier
     */
    public BusIdentifier getBusIdentifier() {
        return mBusIdentifier;
    }

    /**
     * Get the PluginManager of this Bus.
     *
     * @return the PluginManager
     */
    public PluginManager getPluginManager() {
        return mPluginManager;
    }

    /**
     * Get the BridgeConnectorManager of this Bus.
     *
     * @return the BridgeConnectorManager
     */
    public BridgeConnectorManager getBridgeConnectorManager() {
        return mBridgeConnectorManager;
    }

    /**
     * Helper method to send message to the appropriate plugins
     *
     * @param message           the message
     * @param exceptionListener The ExceptionListener to listen for any exception that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    private void sendMessageToPlugins(final Message message, ExceptionListener exceptionListener) {
        final String logAppend = "[Bus:" + mBusIdentifier + "][ID:" + message.getMessageId() + "]";
        log.debug(logAppend + " Starting delivery to plugins");
        log.trace(message.toString());

        ArrayList<Plugin> plugins = mPluginManager.getPluginsForMessage(message);

        for (Plugin plugin : plugins) {
            //Create Acknowledgement as we delivered this Message to the Plugin
            log.info(logAppend + "[Plugin:" + plugin.getPluginIdentifier() + "] Sending Acknowledgement");
            if (message instanceof Request || message instanceof Response) {
                sendMessage(new Acknowledgement(new GlobalIdentifier(mBusIdentifier, plugin.getPluginIdentifier()),
                                message.getSenderIdentifier(),
                                message.getMessageId()),
                        exceptionListener);
            }

            //deliver Message to Plugin
            if (message instanceof Request) {
                sendRequestToPlugin(plugin, (Request) message);
            } else {
                sendResponseToPlugin(plugin, (ResponseMessage) message);
            }
        }

        log.debug(logAppend + " Delivery of message to plugins finished");
    }

    /**
     * Internally used function to send Request to Plugin
     * Required for separation of Core and Synchronization
     *
     * @param plugin  the Plugin to send the Request to
     * @param request The Request to send
     */
    protected void sendRequestToPlugin(Plugin plugin, Request request) {
        final String logAppend = "[Bus:" + mBusIdentifier + "][ID:" + request.getMessageId() + "][Plugin:" + plugin.getPluginIdentifier() + "] ";
        log.info(logAppend + "Processing Request");
        try {
            plugin.processRequest(request);
        } catch (Exception e) {
            log.error(logAppend + "Exception occurred while processing Request", e);
        }
    }

    /**
     * Internally used function to send ResponseMessage to Plugin
     * Required for separation of Core and Synchronization
     *
     * @param plugin          the Plugin to send the ResponseMessage to
     * @param responseMessage The ResponseMessage to send
     */
    protected void sendResponseToPlugin(Plugin plugin, ResponseMessage responseMessage) {
        final String logAppend = "[Bus:" + mBusIdentifier + "][ID:" + ((Message) responseMessage).getMessageId() + "][Plugin:" + plugin.getPluginIdentifier() + "] ";
        log.info(logAppend + "Processing ResponseMessage");
        try {
            plugin.processResponse(responseMessage);
        } catch (Exception e) {
            log.error(logAppend + "Exception occurred while processing ResponseMessage", e);
        }
    }

    /**
     * Pushes a message on the bus.
     * <p>
     * The bus can communicate internally between plugins and externally with all connected buss. The appropriate
     * recipients are selected via querying the recipient field in the message. If no appropriate recipient can be
     * found, the message is dropped.
     *
     * @param message           the message
     * @param exceptionListener The ExceptionListener to listen for any exception that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    private void internalSendMessageHelper(Message message, ExceptionListener exceptionListener) throws RecipientBusNotFoundException, PipeException {
        log.info("[Bus:" + mBusIdentifier + "][ID:" + message.getMessageId() + "] Pushing Message on Bus ");
        log.trace(message.toString());

        GlobalIdentifier recipientIdentifier = message.getRecipientIdentifier();

        if (recipientIdentifier.getBusIdentifier().isGlobalBroadcast()) {
            sendMessageToPlugins(message, exceptionListener);
            mBridgeConnectorManager.outgoingMessage(message);
        } else if (recipientIdentifier.getBusIdentifier().equals(mBusIdentifier))
            sendMessageToPlugins(message, exceptionListener);
        else {
            mBridgeConnectorManager.outgoingMessage(message);
        }

        log.debug("[Bus:" + mBusIdentifier + "][ID:" + message.getMessageId() + "] Pushing Message on Bus finished");
    }

    /**
     * Sends a Message via the Bus.
     * <p>
     * The bus can communicate internally between plugins and externally with all connected buss. The appropriate
     * recipients are selected via querying the recipient field in the message. If no appropriate recipient can be
     * found, the message is dropped.
     *
     * @param message           The message to send
     * @param exceptionListener The ExceptionListener to listen for any exception that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    public void sendMessage(Message message, ExceptionListener exceptionListener) {
        try {
            internalSendMessageHelper(message, exceptionListener);
        } catch (PipeException | RecipientBusNotFoundException e) {
            if (exceptionListener != null)
                exceptionListener.exceptionThrown(e);
        }
    }

    /**
     * Parses a Message and sends it via the Bus.
     * <p>
     * The bus can communicate internally between plugins and externally with all connected buss. The appropriate
     * recipients are selected via querying the recipient field in the message. If no appropriate recipient can be
     * found, the message is dropped.
     *
     * @param messageBytes      The message as unparsed byte array to send
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          ParseMessageException: Thrown when the Message could not be parsed
     *                          </li>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     */
    public void sendMessage(byte[] messageBytes, ExceptionListener exceptionListener) {
        try {
            if (messageBytes.length == 0)
                throw new ParseMessageException();

            log.info("[Bus:" + getBusIdentifier() + "] Incoming Message from ");
            //check the first byte to determine the message type
            Message message;
            switch (messageBytes[0]) {
                case Request.TypeByte:
                    message = Request.restoreFromBytes(messageBytes);
                    break;
                case Response.TypeByte:
                    message = Response.restoreFromBytes(messageBytes);
                    break;
                case Acknowledgement.TypeByte:
                    message = Acknowledgement.restoreFromBytes(messageBytes);
                    break;
                default:
                    throw new ParseMessageException();
            }
            log.debug("[Bus:" + getBusIdentifier() + "] Delivery of incoming Message finished");
            internalSendMessageHelper(message, exceptionListener);
        } catch (Exception e) {
            if (exceptionListener != null)
                exceptionListener.exceptionThrown(e);
        }
    }

    /**
     * Injects Managers into the Bus for testing purposes
     *
     * @param bridgeConnectorManager The BridgeConnectorManager to inject
     * @param pluginManager          The PluginManager to inject
     */
    void injectManagers(BridgeConnectorManager bridgeConnectorManager, PluginManager pluginManager) {
        mBridgeConnectorManager = bridgeConnectorManager;
        mPluginManager = pluginManager;
    }
}
