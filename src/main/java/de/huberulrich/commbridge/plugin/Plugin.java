/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.beans.ExceptionListener;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * A plugin works off and/or sends messages. The appropriate plugin for a message is chosen via matching the plugin
 * identifier (returned via #getPluginIdentifer()) and the available tasks of a plugin with the specified values in the
 * message.
 */
public abstract class Plugin {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(Plugin.class);

    /**
     * Synchronization lock for the bus
     */
    private ReentrantReadWriteLock mBusLock = new ReentrantReadWriteLock();

    /**
     * The bus, via which this plugin can send messages
     * The bus will be set by the PluginManager at registration and unset at deregistration
     */
    private Bus mBus = null;

    private Timer listenerTimer = new Timer();

    /**
     * Map for the listeners
     */
    private final ConcurrentHashMap<UUID, ResponseListener> mResponseListenerHashMap = new ConcurrentHashMap<>();

    /**
     * Map for the listeners
     */
    private final ConcurrentHashMap<UUID, Boolean> mResponseListenerCalledHashMap = new ConcurrentHashMap<>();


    /**
     * Get the globally unique id of this plugin.
     * <p>
     * This id is used to send messages directly and securely to a specific plugin. The PluginManager takes care, that
     * there are no duplicate plugin ids.
     *
     * @return the id
     */
    public abstract PluginIdentifier getPluginIdentifier();

    /**
     * Get the readable name of this plugin
     *
     * @return the readable name
     */
    public abstract String getUserReadableName();

    /**
     * Get the tasks this plugin can handle
     * <p>
     * These tasks are used, to check whether a message can be handled by this plugin.
     *
     * @return array with all tasks
     */
    public abstract Task[] getCapabilities();

    /**
     * Set the bus this plugin shall use for outgoing communication
     *
     * @param bus the bus
     */
    void setBus(Bus bus) {
        mBusLock.writeLock().lock();
        try {
            mBus = bus;
        } finally {
            mBusLock.writeLock().unlock();
        }

    }

    /**
     * Returns the BusIdentifier of the Bus this Plugin is connected to if any.
     *
     * @return the BusIdentifier
     * @throws NotRegisteredException Thrown if not connected to any Bus
     */
    protected BusIdentifier getLocalBusIdentifier() throws NotRegisteredException {
        if (mBus == null)
            throw new NotRegisteredException();
        return mBus.getBusIdentifier();
    }

    /**
     * Sends a Request over the Bus.
     *
     * @param recipientId       The recipient
     * @param task              The task the recipient shall perform
     * @param data              The data needed for the task
     * @param exceptionListener The ExceptionListener to listen for any exception that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @throws NotRegisteredException Thrown when the BridgeConnector is not connected to any bus. This happens
     *                                when it was not registered with any BridgeConnectorManager previously.
     */
    protected void pushRequestToBus(GlobalIdentifier recipientId,
                                    Task task,
                                    byte[] data,
                                    ExceptionListener exceptionListener) throws NotRegisteredException {
        pushRequestToBus(recipientId, task, data, exceptionListener, null, 0);
    }

    /**
     * Sends a Request over the Bus.
     *
     * @param recipientId       The recipient
     * @param task              The task the recipient shall perform
     * @param data              The data needed for the task
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @param responseListener  The listener for acknowledgements to this message
     * @param timeout           The time in milli-seconds after which the ResponseListener is discarded and onTimeout() is called
     * @throws NotRegisteredException Thrown when the BridgeConnector is not connected to any bus. This happens
     *                                when it was not registered with any BridgeConnectorManager previously.
     */
    protected void pushRequestToBus(GlobalIdentifier recipientId,
                                    Task task,
                                    byte[] data,
                                    ExceptionListener exceptionListener,
                                    ResponseListener responseListener,
                                    long timeout) throws NotRegisteredException {
        mBusLock.readLock().lock();
        try {
            if (mBus == null) {
                log.error("[Plugin:" + getPluginIdentifier() + "] Trying to push Request to Bus while not being connected to one");
                throw new NotRegisteredException();
            }
            log.info("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "] Pushing Request to Bus");

            Request message = new Request(new GlobalIdentifier(mBus.getBusIdentifier(), getPluginIdentifier()), recipientId, task, data);

            addListenerToMessage(message.getMessageId(), responseListener, timeout, exceptionListener);

            mBus.sendMessage(message, exceptionListener);

            log.debug("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "] Finished pushing Request to Bus");
        } finally {
            mBusLock.readLock().unlock();
        }
    }

    /**
     * Pushes a Response to a previously received Request to the Bus and attaches a listener for Responses
     *
     * @param request           The Request the new Response is associated to
     * @param data              The data to send with the Response
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @param responseListener  the listener if a Response or Acknowledgement is received to the new Response
     * @param timeout           The time in milli-seconds after which the ResponseListener is discarded and onTimeout() is called
     * @throws NotRegisteredException Thrown if the Plugin is not registered to a Bus
     */
    protected void pushResponseToBus(Request request,
                                     byte[] data,
                                     ExceptionListener exceptionListener,
                                     ResponseListener responseListener,
                                     long timeout) throws NotRegisteredException {
        pushResponeToBus(request, data, exceptionListener, responseListener, timeout);
    }

    /**
     * Pushes a Response to a previously received Response to the Bus and attaches a listener for Responses
     *
     * @param response          The Response the new Response is associated to
     * @param data              The data to send with the Response
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @param responseListener  the listener if a Response or Acknowledgement is received to the new Response
     * @param timeout           The time in milli-seconds after which the ResponseListener is discarded and onTimeout() is called
     * @throws NotRegisteredException Thrown if the Plugin is not registered to a Bus
     */
    protected void pushResponseToBus(Response response,
                                     byte[] data,
                                     ExceptionListener exceptionListener,
                                     ResponseListener responseListener,
                                     long timeout) throws NotRegisteredException {
        pushResponeToBus(response, data, exceptionListener, responseListener, timeout);
    }

    /**
     * Pushes a Response to a previously received Request to the Bus
     *
     * @param request           The Request the new Response is associated to
     * @param data              The data to send with the Response
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @throws NotRegisteredException Thrown if the Plugin is not registered to a Bus
     */
    protected void pushResponseToBus(Request request,
                                     byte[] data,
                                     ExceptionListener exceptionListener) throws NotRegisteredException {
        pushResponeToBus(request, data, exceptionListener, null, 0);
    }

    /**
     * Pushes a Response to a previously received Response to the Bus
     *
     * @param response          The Response the new Response is associated to
     * @param data              The data to send with the Response
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @throws NotRegisteredException Thrown if the Plugin is not registered to a Bus
     */
    protected void pushResponseToBus(Response response,
                                     byte[] data,
                                     ExceptionListener exceptionListener) throws NotRegisteredException {
        pushResponeToBus(response, data, exceptionListener, null, 0);
    }

    /**
     * Internal helper to push a Response to a previously received Message to the Bus
     *
     * @param responseTo        The message the response is associated to
     * @param data              The data of the response
     * @param exceptionListener The ExceptionListener to listen for any exceptions that occur while processing the
     *                          Message. The possible Exceptions are:
     *                          <ul>
     *                          <li>
     *                          RecipientBusNotFoundException: Thrown when the recipient bus was not found
     *                          </li>
     *                          <li>
     *                          PipeException: Thrown when an Exception occurred while pushing the Message through
     *                          the Pipe of the BridgeConnector
     *                          </li>
     *                          </ul>
     * @param responseListener  the listener if a Response or Acknowledgement is received to the new Response
     * @param timeout           The time in milli-seconds after which the ResponseListener is discarded and onTimeout() is called
     * @throws NotRegisteredException Thrown if the Plugin is not registered to a Bus
     */
    private void pushResponeToBus(Message responseTo,
                                  byte[] data,
                                  ExceptionListener exceptionListener,
                                  ResponseListener responseListener,
                                  long timeout) throws NotRegisteredException {
        mBusLock.readLock().lock();
        try {
            if (mBus == null) {
                log.error("[Plugin:" + getPluginIdentifier() + "] Trying to push Response to Bus while not being connected to one");
                throw new NotRegisteredException();
            }
            log.info("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "] Pushing Response to Bus");
            Response response = new Response(new GlobalIdentifier(mBus.getBusIdentifier(), getPluginIdentifier()),
                    responseTo.getSenderIdentifier(),
                    responseTo.getMessageId(),
                    data);

            addListenerToMessage(response.getMessageId(), responseListener, timeout, exceptionListener);

            mBus.sendMessage(response, exceptionListener);

            log.debug("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "] Finished pushing Response to Bus");
        } finally {
            mBusLock.readLock().unlock();
        }
    }

    /**
     * Adds associates a listener to a message, so it is called when an acknowledgement or response is received
     *
     * @param messageId         The id of the Message to associate the listener to
     * @param listener          The ResponseListener to set
     * @param timeout           The time in milli-seconds after which the ResponseListener is discarded and onTimeout() is called
     * @param exceptionListener The ExceptionListener to be notified if an Exception occurs while calling onTimeout()
     */
    private void addListenerToMessage(UUID messageId,
                                      ResponseListener listener,
                                      long timeout,
                                      ExceptionListener exceptionListener) {
        if (messageId != null && listener != null) {
            mResponseListenerHashMap.put(messageId, listener);
            mResponseListenerCalledHashMap.put(messageId, false);

            //Create the TimerTask to delete the listener after 30 seconds
            listenerTimer.schedule(new TimerTask() {
                WeakReference<Plugin> pluginWeakReference = new WeakReference<>(Plugin.this);

                @Override
                public void run() {
                    Plugin plugin = pluginWeakReference.get();
                    if (plugin != null) {
                        log.info("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "][ID:" + messageId + "] Removing listener");
                        //Remove listener
                        ResponseListener responseListener = mResponseListenerHashMap.remove(messageId);
                        Boolean called = mResponseListenerCalledHashMap.remove(messageId);
                        //Check if onTimeout() needs to be called
                        if (responseListener != null && called != null && !called)
                            //call onTimeout()
                            try {
                                log.info("[ID:" + messageId + "] Calling onTimeout() of listener");
                                responseListener.onTimeout();
                            } catch (Exception e) {
                                log.error("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "][ID:" + messageId + "] An exception occurred while calling onTimeout() of listener", e);
                                exceptionListener.exceptionThrown(e);
                            }
                        log.debug("[Bus:" + mBus.getBusIdentifier() + "][Plugin:" + getPluginIdentifier() + "][ID:" + messageId + "] Removed listener");
                    }
                }
            }, timeout);
        }
    }

    /**
     * Handles all requests sent to this plugin.
     * Please take care of concurrency.
     *
     * @param request the request
     */
    public abstract void processRequest(Request request);

    /**
     * Send a response to this plugin to process if a listener for the response exists
     *
     * @param response the response
     */
    public final void processResponse(ResponseMessage response) {
        String logAppend;
        mBusLock.readLock().lock();
        try {
            logAppend = "[Bus:" + mBus.getBusIdentifier() + "][ID:" + ((Message) response).getMessageId() + "][Plugin:" + getPluginIdentifier() + "] ";
        } finally {
            mBusLock.readLock().unlock();
        }

        log.info(logAppend + "Received Response to ID:" + response.getOriginalMessageId());

        log.trace(response.toString());
        ResponseListener listener = mResponseListenerHashMap.get(response.getOriginalMessageId());

        //disable timeout call since a response was received
        mResponseListenerCalledHashMap.put(response.getOriginalMessageId(), true);
        if (listener != null) {
            log.debug(logAppend + "Listener attached to ID:" + response.getOriginalMessageId());
            try {
                if (response instanceof Response)
                    listener.onResponseReceived((Response) response);
                else
                    listener.onAcknowledgementReceived((Acknowledgement) response);
            } catch (Exception e) {
                log.error(logAppend + "Exception occurred while processing Response to ID:" + response.getOriginalMessageId(), e);
            }
        } else
            log.debug(logAppend + "No listener attached to ID:" + response.getOriginalMessageId());
    }

    @Override
    public String toString() {
        return "Plugin\n" +
                "\tReadable Name: " + getUserReadableName() + "\n" +
                "\tPluginIdentifier: " + getPluginIdentifier() + "\n" +
                "\tCapabilities: " + Arrays.toString(getCapabilities()) + "\n";
    }
}
