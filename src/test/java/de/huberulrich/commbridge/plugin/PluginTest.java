/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.GlobalIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class PluginTest {
    @Test
    public void testSetBus() throws NotRegisteredException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), new byte[]{}, null);
        verify(bus).sendMessage(any(Request.class), any());
    }

    @Test(expected = NotRegisteredException.class)
    public void testGetLocalBusIdentifier() throws NotRegisteredException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        assertEquals(new BusIdentifier("mockBus"), plugin.getLocalBusIdentifier());

        plugin.setBus(null);
        plugin.getLocalBusIdentifier();
    }

    @Test(expected = NotRegisteredException.class)
    public void testSetBusWhenNotRegistered() throws NotRegisteredException {
        Bus bus = mock(Bus.class);
        PluginImpl plugin = new PluginImpl();

        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), new byte[]{}, null);
        verify(bus).sendMessage(any(Request.class), any());
    }

    @Test
    public void testPushRequestToBus() throws NotRegisteredException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), new byte[]{}, null);
        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), null, null);
        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), null, null, null, 0);
        verify(bus, times(3)).sendMessage(any(Request.class), any());
    }

    @Test
    public void testPushResponseToBus() throws NotRegisteredException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        Request request = mock(Request.class);
        doReturn(UUID.randomUUID()).when(request).getMessageId();
        doReturn(new GlobalIdentifier("a:b:c")).when(request).getSenderIdentifier();

        plugin.pushResponseToBus(request, null, null);
        plugin.pushResponseToBus(request, new byte[]{}, null);
        plugin.pushResponseToBus(request, new byte[]{}, null, null, 0);
        plugin.pushResponseToBus(request, new byte[]{}, null, new ResponseListener() {
            @Override
            public void onAcknowledgementReceived(Acknowledgement acknowledgement) {

            }

            @Override
            public void onResponseReceived(Response response) {

            }

            @Override
            public void onTimeout() {

            }
        }, 200);


        Response response = mock(Response.class);
        doReturn(UUID.randomUUID()).when(response).getMessageId();
        doReturn(new GlobalIdentifier("a:b:c")).when(response).getSenderIdentifier();

        plugin.pushResponseToBus(response, null, null);
        plugin.pushResponseToBus(response, new byte[]{}, null);
        plugin.pushResponseToBus(response, new byte[]{}, null, null, 0);
        plugin.pushResponseToBus(response, new byte[]{}, null, new ResponseListener() {
            @Override
            public void onAcknowledgementReceived(Acknowledgement acknowledgement) {

            }

            @Override
            public void onResponseReceived(Response response) {

            }

            @Override
            public void onTimeout() {

            }
        }, 200);
        verify(bus, Mockito.times(8)).sendMessage(any(Message.class), any());
    }

    @Test
    public void testProcessResponse() throws NotRegisteredException, InterruptedException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        ResponseListener responseListener = spy(new ResponseListener() {
            @Override
            public void onAcknowledgementReceived(Acknowledgement acknowledgement) {

            }

            @Override
            public void onResponseReceived(Response response) {

            }

            @Override
            public void onTimeout() {

            }
        });

        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), new byte[]{}, null, responseListener, 200);
        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus).sendMessage(captor.capture(), any());
        UUID uuid = captor.getValue().getMessageId();

        Acknowledgement acknowledgement = new Acknowledgement(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("mockBus:mock:plugin"), uuid);
        plugin.processResponse(acknowledgement);
        verify(responseListener).onAcknowledgementReceived(acknowledgement);

        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("mockBus:mock:plugin"), uuid, new byte[]{});
        plugin.processResponse(response);
        verify(responseListener).onResponseReceived(response);

        Thread.sleep(250);
        verify(responseListener, never()).onTimeout();


        //ensure that listener is never called again
        plugin.processResponse(response);
        //verify that listener was only called the time before and not a second time
        verify(responseListener).onResponseReceived(response);
    }

    @Test
    public void testProcessResponseListenerTimeout() throws NotRegisteredException, InterruptedException {
        Bus bus = mock(Bus.class);
        doReturn(new BusIdentifier("mockBus")).when(bus).getBusIdentifier();
        PluginImpl plugin = new PluginImpl();

        //set the Bus like PluginManager does
        plugin.setBus(bus);

        ResponseListener responseListener = spy(new ResponseListener() {
            @Override
            public void onAcknowledgementReceived(Acknowledgement acknowledgement) {

            }

            @Override
            public void onResponseReceived(Response response) {

            }

            @Override
            public void onTimeout() {

            }
        });

        plugin.pushRequestToBus(new GlobalIdentifier("a:b:c"), new Task("a"), new byte[]{}, null, responseListener, 200);
        ArgumentCaptor<Message> captor = ArgumentCaptor.forClass(Message.class);
        verify(bus).sendMessage(captor.capture(), any());
        UUID uuid = captor.getValue().getMessageId();

        Thread.sleep(250);
        verify(responseListener).onTimeout();

        //ensure that listener is never called again
        Response response = new Response(new GlobalIdentifier("a:b:c"), new GlobalIdentifier("mockBus:mock:plugin"), uuid, new byte[]{});
        plugin.processResponse(response);
        //verify that listener was never invoked
        verify(responseListener, never()).onResponseReceived(response);
    }

    private class PluginImpl extends Plugin {

        @Override
        public PluginIdentifier getPluginIdentifier() {
            return new PluginIdentifier("mock:plugin");
        }

        @Override
        public String getUserReadableName() {
            return null;
        }

        @Override
        public Task[] getCapabilities() {
            return new Task[0];
        }

        @Override
        public void processRequest(Request request) {

        }
    }
}
