/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.bridge;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

@RunWith(MockitoJUnitRunner.class)
public class BridgeConnectorPriorityComparatorTest {
    @Test
    public void testOrderOfPrioritiesRight() {
        BridgeConnectorPriorityComparator comparator = new BridgeConnectorPriorityComparator(0);
        BridgeConnector b1 = mock(BridgeConnector.class);
        BridgeConnector b2 = mock(BridgeConnector.class);

        doReturn(BridgeConnectorPriority.EFFICIENT_FAST).when(b1).getPriority(anyInt());
        doReturn(BridgeConnectorPriority.EFFICIENT_SLOW).when(b2).getPriority(anyInt());
        assertEquals(-1, comparator.compare(b1, b2));

        doReturn(BridgeConnectorPriority.EFFICIENT_SLOW).when(b1).getPriority(anyInt());
        doReturn(BridgeConnectorPriority.INEFFICIENT_FAST).when(b2).getPriority(anyInt());
        assertEquals(-1, comparator.compare(b1, b2));

        doReturn(BridgeConnectorPriority.INEFFICIENT_FAST).when(b1).getPriority(anyInt());
        doReturn(BridgeConnectorPriority.INEFFICIENT_SLOW).when(b2).getPriority(anyInt());
        assertEquals(-1, comparator.compare(b1, b2));
    }
}
