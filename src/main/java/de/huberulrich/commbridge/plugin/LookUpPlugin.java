/*
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 */
package de.huberulrich.commbridge.plugin;

import de.huberulrich.commbridge.bridge.BridgeConnectorManager;
import de.huberulrich.commbridge.bus.Bus;
import de.huberulrich.commbridge.exceptions.NotRegisteredException;
import de.huberulrich.commbridge.identifier.BusIdentifier;
import de.huberulrich.commbridge.identifier.PluginIdentifier;
import de.huberulrich.commbridge.message.Request;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.StringJoiner;

/**
 * Plugin that enables other Plugins to acquire knowledge about the known external Buses, attached Plugins and
 * Capabilities of all Plugins attached to the local Bus.
 * <p>
 * To obtain:
 * <ul>
 * <li>All BusIdentifiers of the known external Buses use {@link #lookUpKnownExternalBusesTask} for your Request</li>
 * <li>All Plugins attached to the local Bus use {@link #lookUpPluginsTask} for your Request</li>
 * <li>All Capabilities of a locally attached Plugin use {@link #lookUpCapabilitiesTask} for your Request and attach the
 * PluginIdentifier of the Plugin you want to query as data. Use {@link PluginIdentifier#getBytes()} to transform the
 * PluginIdentifier.</li>
 * </ul>
 * <p>
 * If the requirements are not met, or the the {@link Request} came from an external Plugin and responding to external
 * Plugins was prohibited, there is no Response sent back.
 */
public class LookUpPlugin extends Plugin {
    /**
     * For internal logging
     */
    private final Log log = LogFactory.getLog(LookUpPlugin.class);

    private PluginManager mPluginManager;
    private BridgeConnectorManager mBridgeConnectorManager;
    private boolean mEnableExternalQueries;

    //Tasks
    /**
     * Task to look up all external Buses a route is known too via
     * {@link de.huberulrich.commbridge.bridge.BridgeConnector}s
     */
    public static final Task lookUpKnownExternalBusesTask = new Task("lookUpKnownExternalBuses");

    /**
     * Task to look up all locally registered {@link Plugin}s
     */
    public static final Task lookUpPluginsTask = new Task("lookUpPlugins");

    /**
     * Task to look up all Capabilities ({@link #getCapabilities()}) of a locally registered {@link Plugin}
     */
    public static final Task lookUpCapabilitiesTask = new Task("lookUpCapabilitiesOfPlugin");

    /**
     * Task to look up registered Plugins that list a given {@link Task} in their Capabilities
     */
    public static final Task lookUpPluginsWithCapabilityTask = new Task("lookUpPluginsWithCapabilityTask");

    /**
     * Creates a {@link LookUpPlugin}
     *
     * @param pluginManager          The {@link PluginManager} this Plugin is attached to
     * @param bridgeConnectorManager The {@link BridgeConnectorManager} attached to the same {@link Bus} this Plugin is
     *                               attached to
     * @param enableExternalQueries  Whether to allow external plugins to request data of this Bus (potential security
     *                               risk, use this carefully)
     */
    public LookUpPlugin(PluginManager pluginManager, BridgeConnectorManager bridgeConnectorManager, boolean enableExternalQueries) {
        if (pluginManager == null || bridgeConnectorManager == null)
            throw new IllegalArgumentException("Arguments must not be null");
        mPluginManager = pluginManager;
        mBridgeConnectorManager = bridgeConnectorManager;
        mEnableExternalQueries = enableExternalQueries;
    }

    /**
     * Allows external Plugins (Plugins attached to a Bus that is not the local one) to query data of this Bus (the
     * local one). Be careful with this setting, since it can cause leakage of sensitive information.
     */
    public void allowExternalQueries() {
        mEnableExternalQueries = true;
    }

    /**
     * Disallows external Plugins (Plugins attached to a Bus that is not the local one) to query data of this Bus (the
     * local one).
     */
    public void disallowExternalQueries() {
        mEnableExternalQueries = false;
    }

    @Override
    public PluginIdentifier getPluginIdentifier() {
        return new PluginIdentifier("commBridge:lookUp");
    }

    @Override
    public String getUserReadableName() {
        return "LookUpPlugin by CommBridge";
    }

    @Override
    public Task[] getCapabilities() {
        return new Task[]{lookUpKnownExternalBusesTask, lookUpPluginsTask, lookUpCapabilitiesTask, lookUpPluginsWithCapabilityTask};
    }

    @Override
    public void processRequest(Request request) {
        byte[] requestData = request.getData();
        try {
            //restrict queries to local senders if necessary
            if (mEnableExternalQueries || request.getSenderIdentifier().getBusIdentifier().equals(getLocalBusIdentifier())) {
                if (request.getTask().equals(lookUpKnownExternalBusesTask)) {
                    //return all known external Buses connected via BridgeConnectors to this Bus
                    StringJoiner stringJoiner = new StringJoiner(",");
                    for (BusIdentifier busIdentifier : mBridgeConnectorManager.getAllKnownExternalBusIdentifiers()) {
                        stringJoiner.add(busIdentifier.toString());
                    }

                    byte[] data = stringJoiner.toString().getBytes(StandardCharsets.UTF_8);
                    pushResponseToBus(request, data,
                            e -> log.error("[LookUpPlugin][ID:" + request.getMessageId() + "] Exception occurred while sending response to lookUpPluginsTask"));

                } else if (request.getTask().equals(lookUpPluginsTask)) {
                    //return all Plugins registered to this Bus
                    StringJoiner stringJoiner = new StringJoiner(",");
                    for (Plugin plugin : mPluginManager.getAllRegisteredPlugins()) {
                        stringJoiner.add(plugin.getPluginIdentifier().toString());
                    }

                    byte[] data = stringJoiner.toString().getBytes(StandardCharsets.UTF_8);
                    pushResponseToBus(request, data,
                            e -> log.error("[LookUpPlugin][ID:" + request.getMessageId() + "] Exception occurred while sending response to lookUpPluginsTask"));

                } else if (request.getTask().equals(lookUpCapabilitiesTask) && requestData != null) {
                    //return all Capabilities of the specified Plugin if it is registered
                    String requestString = new String(requestData, StandardCharsets.UTF_8);
                    if (requestString.matches("(?:[A-Za-z0-9]+:[A-Za-z0-9]+)$")) {
                        PluginIdentifier pluginIdentifier = new PluginIdentifier(requestString);
                        Plugin plugin = mPluginManager.getRegisteredPlugin(pluginIdentifier);
                        if (plugin != null) {
                            StringJoiner stringJoiner = new StringJoiner(",");
                            for (Task task : plugin.getCapabilities())
                                stringJoiner.add(task.toString());

                            byte[] data = stringJoiner.toString().getBytes(StandardCharsets.UTF_8);
                            pushResponseToBus(request, data,
                                    e -> log.error("[LookUpPlugin][ID:" + request.getMessageId() + "] Exception occurred while sending response to lookUpCapabilitiesTask"));
                        }
                    }
                } else if (request.getTask().equals(lookUpPluginsWithCapabilityTask) && requestData != null) {
                    //return all Plugins that are capable of handling the specified Task
                    String requestString = new String(requestData, StandardCharsets.UTF_8);
                    if (requestString.matches("^(?:[A-Za-z0-9]+)$")) {
                        Task task = new Task(requestString);
                        Collection<Plugin> plugins = mPluginManager.getPluginsForTask(task);
                        StringJoiner stringJoiner = new StringJoiner(",");
                        for (Plugin plugin : plugins)
                            stringJoiner.add(plugin.getPluginIdentifier().toString());

                        byte[] data = stringJoiner.toString().getBytes(StandardCharsets.UTF_8);
                        pushResponseToBus(request, data,
                                e -> log.error("[LookUpPlugin][ID:" + request.getMessageId() + "] Exception occurred while sending response to lookUpPluginsWithCapabilityTask"));
                    }
                }
            }
        } catch (NotRegisteredException e) {
            log.error("[LookUpPlugin][ID:" + request.getMessageId() + "] Received Request while not being attached to Bus");
        }
    }

    /**
     * Convenience method to parse the Response of a {@link #lookUpKnownExternalBusesTask} to an array of
     * {@link BusIdentifier}s
     *
     * @param data the unparsed data returned from the request
     * @return the array with the BusIdentifiers
     */
    public static ArrayList<BusIdentifier> parseResponseForLookUpKnownExternalBusesTask(byte[] data) {
        if (data == null)
            throw new IllegalArgumentException();

        String[] identifiers = new String(data, StandardCharsets.UTF_8).split(",");
        ArrayList<BusIdentifier> busIdentifiers = new ArrayList<>(identifiers.length);
        for (String identifier : identifiers) {
            busIdentifiers.add(new BusIdentifier(identifier));
        }
        return busIdentifiers;
    }

    /**
     * Convenience method to parse the Response of a {@link #lookUpPluginsTask} to an array of
     * {@link PluginIdentifier}s
     *
     * @param data the unparsed data returned from the request
     * @return the array with the PluginIdentifiers
     */
    public static ArrayList<PluginIdentifier> parseResponseForLookUpPluginsTask(byte[] data) {
        return parseDelimitedPluginIdentifiersByteArray(data);
    }

    /**
     * Convenience method to parse the Response of a {@link #lookUpCapabilitiesTask} to an array of
     * {@link Task}s
     *
     * @param data the unparsed data returned from the request
     * @return the array with the Tasks
     */
    public static ArrayList<Task> parseResponseForLookUpCapabilitiesTask(byte[] data) {
        if (data == null)
            throw new IllegalArgumentException();

        String[] identifiers = new String(data, StandardCharsets.UTF_8).split(",");
        ArrayList<Task> tasks = new ArrayList<>(identifiers.length);
        for (String identifier : identifiers) {
            tasks.add(new Task(identifier));
        }
        return tasks;
    }

    /**
     * Convenience method to parse the Response of a {@link #lookUpPluginsWithCapabilityTask} to an array of
     * {@link PluginIdentifier}s
     *
     * @param data the unparsed data returned from the request
     * @return the array with the PluginIdentifiers
     */
    public static ArrayList<PluginIdentifier> parseResponseForLookUpPluginsWithCapabilityTask(byte[] data) {
        return parseDelimitedPluginIdentifiersByteArray(data);
    }

    private static ArrayList<PluginIdentifier> parseDelimitedPluginIdentifiersByteArray(byte[] data) {
        if (data == null)
            throw new IllegalArgumentException();

        String[] identifiers = new String(data, StandardCharsets.UTF_8).split(",");
        ArrayList<PluginIdentifier> pluginIdentifiers = new ArrayList<>(identifiers.length);
        for (String identifier : identifiers) {
            pluginIdentifiers.add(new PluginIdentifier(identifier));
        }
        return pluginIdentifiers;
    }
}
